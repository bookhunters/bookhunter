import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';
// import BookInfo from './BookInfo'
import { useNavigate } from "react-router-dom";
import Box from '@mui/material/Box';


const BookCard = (props) => {
    const navigate = useNavigate();
    return (
        <Box m={0} pt={1}>
        <Card sx={{ width: 500 }} style={{flex: 1}}>
            <CardContent style={{textAlign: "center"}}>
                <Typography variant="h5" component="div">
                    {props.title}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    {props.author}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    {props.genre}
                </Typography>
                <Typography variant="body2">
                   Language: {props.lang}
                </Typography>
                <Typography variant="body2">
                   Published: {props.date}
                </Typography>
                <Typography variant="body2">
                   Pages: {props.count}
                </Typography>
                <Typography variant="body2">
                   Ratings: {props.ratings}
                </Typography>
            </CardContent>
            <CardActions>
                <Button variant="text"  onClick={() => navigate("/bookinfo", {state: {book_title : props.title}})}>Learn More</Button>
            </CardActions>
        </Card>
        </Box>
    );
}

export default BookCard;