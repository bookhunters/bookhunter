import React from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';

// THIS IS GOING TO BE USED IN THE LATER PHASE
// FOR NOW WE WILL HAVE TO HARD CODE THIS


const AuthorInfo = (props) => {
    const navigate = useNavigate();
    return (
        <div style={{justifyContent: 'center',}}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableBody>
                        <TableCell>Birth Date: {props.birthdate}</TableCell>
                        <TableCell>From: {props.origin}</TableCell>
                        <TableCell>Language: {props.language}</TableCell>
                        <TableCell>Genre: {props.genre}</TableCell>
                        <TableCell>Books: {props.books}</TableCell>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default AuthorInfo;