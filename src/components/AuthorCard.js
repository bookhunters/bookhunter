import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
// import BookInfo from './BookInfo'
import { useNavigate } from "react-router-dom";
import Box from '@mui/material/Box';

const AuthorCard = (props) => {
    const navigate = useNavigate();
    return (
        <Box m={0} pt={1}>
        <Card sx={{ width: 600 }} style={{flex: 1}}>
            <CardContent style={{textAlign: "center"}}>
                <Typography variant="h5" component="div">
                    {props.name}
                </Typography>
                <br></br>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    Nationality: {props.language}
                </Typography>
                <Typography variant="body2">
                   Born: {props.birth_date}
                </Typography>
                <br></br>
                <Typography variant="body2">
                   Genres: {props.genres_showup}
                </Typography>
                <br></br>
                <Typography variant="body2">
                   Books: {props.books_showup}
                </Typography>
            </CardContent>
            <CardActions>
            <Button variant="text"  onClick={() => navigate("/authorinfo", {state: {author_name : props.name}})}>Learn More</Button>
            </CardActions>
        </Card>
        </Box>
    );
}

export default AuthorCard;