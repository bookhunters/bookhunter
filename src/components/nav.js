import React, { Component } from 'react'; 
import 'bootstrap/dist/css/bootstrap.css';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Container } from 'react-bootstrap';
import Logo from "../../src/assets/logo-1.jpg";

const NavbarComponent = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="./home"><img src={Logo} style={{display: "flex", maxHeight: 75, minHeight: 50}}></img></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="./home">Home</Nav.Link>
                        <Nav.Link href="./genres">Genres</Nav.Link>
                        <Nav.Link href="./authors">Authors</Nav.Link>
                        <Nav.Link href='./books'>Books</Nav.Link>
                        <Nav.Link href="./models">Models</Nav.Link>
                        <Nav.Link href="./othermodel">Other Models</Nav.Link>
                        <Nav.Link href="./about">About</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavbarComponent;