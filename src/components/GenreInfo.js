import React from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';

// THIS IS GOING TO BE USED IN THE LATER PHASE
// FOR NOW WE WILL HAVE TO HARD CODE THIS


const GenreInfo = (props) => {
    
    return (
        <div style={{justifyContent: 'center',}}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableBody>
                        <TableCell>Notable Authors: {props.notableauthors}</TableCell>
                        <TableCell>Description: {props.description}</TableCell>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default GenreInfo;