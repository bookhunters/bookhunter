import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
// import BookInfo from './BookInfo'
import { useNavigate } from "react-router-dom";
import Box from '@mui/material/Box';


const GenreCard = (props) => {
    const navigate = useNavigate();
    return (
        <Box m={0} pt={1}>
        <Card sx={{ width: 600 }} style={{flex: 1}} variant="outlined">
            <CardContent style={{textAlign: "center"}}>
                <Typography variant="h5" component="div">
                    {props.genre_name}
                </Typography>
                <br></br>
                <Typography variant="body2">
                    Authors: {props.authors}
                </Typography>
                <br></br>
                <Typography variant="body2">
                    Books: {props.books}
                </Typography>
                <br></br>
                <Typography variant="body2">
                    Profitability: {props.profit}
                </Typography>
            </CardContent>
            <CardActions>
                <Button variant="text"  onClick={() => navigate("/genreinfo", {state: {genre_name : props.genre_name}})}>Learn More</Button>
            </CardActions>
        </Card>
        </Box>
    );
}

export default GenreCard;