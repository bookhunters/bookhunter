import React from 'react';
import './App.css';
import NavbarComponent from './components/nav';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/home';
import Genres from './pages/genres';
import About from './pages/about';
import Authors from './pages/authors';
import Books from './pages/books';
import Models from './pages/models';
import Romance from './pages/romance';
import BookTemplate from './pages/bookTemplate';
import OtherModels from './pages/othermodel'
import AuthorTemplate from './pages/AuthorTemplate';
import GenreTemplate from './pages/GenreTemplate'


// Make sure to add the page imports to use them in the router below!!

  
function App() {
  return (
    <Router>
      <div id='root'/>
      <NavbarComponent />
      <Routes>
        <Route path='/' element = { <Home/> }/>
        <Route path='/home' element={ <Home/> } />
        <Route path = '/authors' element={ <Authors/>}/>
        <Route path = '/books' element={ <Books/> } />
        <Route path='/about' element={ <About/> } />
        <Route path= '/models' element={ <Models/> } />
        <Route path='/genres' element={ <Genres/> } />
        <Route path='/romance' element={ <Romance/> } /> 
        <Route path='/bookinfo' element={<BookTemplate/>}/>
        <Route path='/othermodel' element={<OtherModels/>}/>
        <Route path='/authorinfo' element={<AuthorTemplate/>}/>
        <Route path='/genreinfo' element={<GenreTemplate/>}/>
      </Routes>
    </Router>
  );
}
  
export default App;