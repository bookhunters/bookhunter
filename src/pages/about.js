import React from 'react';
import kenneth from "../../src/assets/Kenneth.jpg";
import alyssa from "../../src/assets/Alyssa.jpg";
import aditya from "../../src/assets/Aditya.jpg";
import yizhou from "../../src/assets/Yizhou.jpg";
import revanth from "../../src/assets/Revanth.jpg";
import aman from "../../src/assets/Aman.jpeg"
import { Button, Image } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import { display, margin, textAlign } from '@mui/system';
document.body.style = 'background: lightblue;';


const About = () => {
    return (
        <div>
            <div style={{fontSize: 100, textAlign: 'center'}}>About Page</div>
            <div style={{fontSize: 50, textAlign: 'center'}}>IDB 4</div>
            <Accordion>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>Meet The Team</div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <row>
                            <div style={{display: "flex", justifyContent: "space-evenly"}}>                  
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={kenneth} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Kenneth</Card.Title>
                                        <Card.Text>
                                            <div>
                                                I am an upcoming junior computer science major at the University of Texas at Austin. I worked mainly on the frontend of this website while learning lots of new skills in the process. In my free time I enjoy hanging out with friends and watching sports and movies.
                                                <div>
                                                    <br></br>Responsibilities: Frontend
                                                    <div>
                                                        Number of Commits: 53
                                                        <div>
                                                            Number of Issues: 6
                                                            <div>
                                                                Number of Tests: 0
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={alyssa} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Alyssa</Card.Title>
                                        <Card.Text>
                                            <div>
                                            I am Alyssa and I am a 4th year student at the University of Texas at Austin. I have the most experience in front-end development but am in the process of learning back-end development. My hobbies include gardening, tennis, and taking care of my cat.
                                                <div>
                                                    <br></br>Responsibilities: Frontend
                                                    <div>
                                                        Number of Commits: 13
                                                        <div>
                                                            Number of Issues: 1
                                                            <div>
                                                                Number of Tests: 0
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={aditya} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Aditya</Card.Title>
                                        <Card.Text>
                                            <div>
                                            I am a rising Senior at UT Austin studying Computer Science. I am excited to be a member of this project as part of the Software Engineering class this summer.
                                                <div>
                                                    <br></br>Responsibilities: Backend
                                                    <div>
                                                        Number of Commits: 0
                                                        <div>
                                                            Number of Issues: 1
                                                            <div>
                                                                Number of Tests: 6
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={yizhou} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Yizhou</Card.Title>
                                        <Card.Text>
                                            <div>
                                            I am a current senior at the University of Texas at Austin. I major in computer science and will graduate in Fall 2023. I have interests in software engineering, computer systems, and social psychology. I feel curious about computer security. When I am free, I like raising animals and visiting different places.
                                                <div>
                                                    <br></br>Responsibilities: Backend API
                                                    <div>
                                                        Number of Commits: 276
                                                        <div>
                                                            Number of Issues: 49
                                                            <div>
                                                                Number of Tests: 10
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={revanth} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Revanth</Card.Title>
                                        <Card.Text>
                                            <div>
                                            I am a senior at the University of Texas at Austin majoring in Computer Science with a minor in Business. In my free time, I like listening to music and playing video games.
                                                <div>
                                                    <br></br>Responsibilities: Frontend, GCP
                                                    <div>
                                                        Number of Commits: 30
                                                        <div>
                                                            Number of Issues: 8
                                                            <div>
                                                                Number of Tests: 0
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card style={{style: 'flex', width: 250}}>
                                    <Card.Img variant="top" src={aman} style= {{maxWidth: 250, maxHeight: 300}}/>
                                    <Card.Body>
                                        <Card.Title style={{textAlign: 'center'}}>Aman</Card.Title>
                                        <Card.Text>
                                            <div>
                                                I am currently enrolled in my 4th year as a computer science major at UT Austin. I am excited to be learning software engineering and the various tools of development.
                                                <div> 
                                                    <br></br>Responsibilities: Frontend
                                                    <div>
                                                        Number of Commits: 6
                                                        <div>
                                                            Number of Issues: 1
                                                            <div>
                                                                Number of Tests: 0
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        </row>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>Project Stats</div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <div style={{fontSize: 30, textAlign: 'center'}}>
                            Total Commits: 378
                            <div>
                                Total Issues: 68
                                <div>
                                    Total Unit Tests: 16
                                    <div>
                                        <a href="https://documenter.getpostman.com/view/21599110/UzBqqRFd">Postman API</a>
                                        <div>
                                            <a href="https://gitlab.com/bookhunters/bookhunter/-/issues">GitLab Issue Tracker</a>
                                            <div>
                                                <a href="https://gitlab.com/bookhunters/bookhunter/-/tree/main">GitLab Repo</a>
                                                <div>
                                                    <a href="https://gitlab.com/bookhunters/bookhunter/-/wikis/home">GitLab Wiki</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>Data Sources</div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <div style={{style: "flex", textAlign: "center", fontSize: 30}}>
                            <a href=" https://openlibrary.org/developers/api">Open Library</a>
                            <br></br>
                            <a href="https://www.mediawiki.org/wiki/API:Main_page">Wikipedia</a>
                            <br></br>
                            <a href="https://developers.google.com/books/docs/overview">Google Books</a>
                            <br></br>
                            <a href="https://developers.google.com/youtube/v3">YouTube</a> 
                            <br></br>
                            <a href="https://developers.google.com/maps">Google Maps</a>
                        </div>
                    </Accordion.Body>
                </Accordion.Item>

                <Accordion.Item eventKey="3">
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>Tools Used</div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <div style={{style: "flex", textAlign: "center", fontSize: 30}}>
                        React - Styling and formattng for JavaScript files.
                        <br></br>
                        Flask - API Implementation
                        <br></br>
                        Groupme - Communicate with each other and schedule meetings.
                        <br></br>
                        Slack - Communicate about updates and our current progress.
                        <br></br>
                        Gitlab - Where our repository is located.
                        <br></br>
                        Postman - Designed our APIs and wrote the documentation for these APIs
                        <br></br>
                        GCP - Service used to host our website.
                        <br></br>
                        Namecheap - Naming service.
                        <br></br>
                        Docker - Open platform for developing, shipping, and running applications
                        <br></br>
                        SQLAlchemy - Library that facilitates the communication between Python programs and databases
                        <br></br>
                        RESTful API - Conforms APIs to a specific architectural style
                        <br></br>
                        yUML - A standard notation for the modeling of real-world objects.
                        </div>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey='5'>
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>
                            Unit Tests
                        </div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <div style={{textAlign: "center"}}>
                            <button style={{fontSize: 30}}>Click to Run Tests</button>
                            <br></br>
                            Tests are not linked to the button but they exist in tests.py and run correctly
                            <br></br>
                            ................
                            <br></br>
                            ----------------------------------------------------------------------
                            <br></br>
                            Ran 16 tests in 1.111s
                        </div>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey='6'>
                    <Accordion.Header>
                        <div style={{fontSize: 30}}>
                            Presentation 
                        </div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <div style={{textAlign: "center", fontSize: 30}}>
                            <a href="https://speakerdeck.com/kcknox/bookhunter">Slide Deck</a>
                        </div>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </div >
    );
}
export default About;
