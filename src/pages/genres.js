import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate';
import GenreCard from '../components/GenreCard';
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Pagination from '@mui/material/Pagination';

const Genres = () => {
    const [genreList, setGenreList] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [currentURL, setCurrentURL] = useState("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=1");
    const [genreSearch, setGenreSearch] = useState("");
    const [genreSort, setGenreSort] = useState("");
    const [numItems, setNumItems] = useState(0);
    const [sortMethod, setSortMethod] = useState(0);
    
    useEffect(() => {
        axios.get(currentURL)
            .then(res => {
                console.log(res.data)
                setGenreList(res.data.map(genre => {
                    return(
                        <GenreCard
                        genre_name = {genre.genre_name}
                        authors= {genre.authors_showup}
                        books= {genre.books_showup}
                        profit= {genre.genre_profitability}
                        key={genre.book_id}
                        id={genre.book_id}
                        />
                    )
                }))
                setNumItems(res.data.length)
            })
            .catch(err => console.log(err))
            urlForPage()
    }, [currentURL])

    function urlForPage() {
        return "https://mediumdb-356314.uc.r.appspot.com/api/genres?page=" + (currentPage + 1) + "&q=" + genreSearch
    }

    const handleChange = (event, value) => {
        setCurrentPage(value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=" + value + "&q=" + genreSearch + "&sort=" + genreSort)
        console.log(currentURL);
    };

    let handleSearch = (e) => {
        setGenreSearch(e.target.value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=1&q=" + genreSearch);
    }

    function handleSort(sortBy) {
        console.log(sortBy);
        if (sortBy == sortMethod){
            setSortMethod(0);
            setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=1&q=" + genreSearch);
            setCurrentPage(1)
            return;
        }
        let sortString = "";
        switch (sortBy) {
            case 1:
                setSortMethod(1);
                sortString = "name"
                break;
            case 2:
                setSortMethod(2);
                sortString = "profits"
                break;
            case 3:
                setSortMethod(3);
                sortString = "authors"
                break;
            case 4:
                setSortMethod(4);
                sortString = "books"
                break;
            case 5:
                setSortMethod(5);
                sortString = "id"
                break;
            default:
                break;
        }
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=1&q=" + genreSearch + "&sort=" + sortString);
        setCurrentPage(1)
        setGenreSort(sortString)
    }

    return(
        <div className="Genre">
            <h1 style= {{fontSize: 50}}>Genres</h1>
            <br></br> 
            <div className="search">
                <TextField
                    id="outlined-basic"
                    onChange={handleSearch}
                    variant="outlined"
                    fullWidth
                    label="Search"
                />
            </div>
            <br></br>
            <div style={{fontSize: 20, textAlign: "center"}}>
                SORT BY:
                <ButtonGroup variant="contained" aria-label="sorting buttons">
                    <Button onClick={() => handleSort(1)}>Name</Button>
                    <Button onClick={() => handleSort(2)}>Profits</Button>
                    <Button onClick={() => handleSort(3)}>Authors</Button>
                    <Button onClick={() => handleSort(4)}>Books</Button>
                    <Button onClick={() => handleSort(5)}>ID</Button>
                </ButtonGroup>
            </div>
            <br></br>
            {genreList}
            <br></br>
            <Pagination
                count={3}
                page={currentPage}
                onChange={handleChange}
            />
        </div>
    )

}
export default Genres;