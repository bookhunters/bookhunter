import "./styles.css";
//import styled from "styled-components";
//import { ImageBackground, StyleSheet, Text, View } from "react-native";
import Library from "../../src/assets/Library.jpg";
import React, { useEffect, useState } from 'react'
import { selectUnstyledClasses } from "@mui/base";
import { useNavigate } from "react-router-dom";
import Button from '@mui/material/Button';



export default function Home() {

  const navigate = useNavigate();

  return (
    
    <div className="App" id='home'>
      <h1 style= {{fontSize: 70}}>Find the Book You Are Hunting!</h1> 
      <br></br>
      <div style= {{fontSize: 25}}>
      Search For A 
      <br></br>
      <Button variant="contained" onClick={() => navigate("/books")}> BOOK </Button>
      <br></br>
      <Button variant="contained" onClick={() => navigate("/authors")}> AUTHOR </Button>
      <br></br>
      <Button variant="contained" onClick={() => navigate("/genres")}> GENRE </Button>
      <br></br>
      <br></br>
      </div>
      <img src={Library}></img>
    </div>
  );
}