import React from 'react';
import "./styles.css";
import SearchBar from "material-ui-search-bar";
import styled from "styled-components";

export const StyledSearchBar = styled(SearchBar)`
  margin: 0 auto;
  max-width: 800px;
`;

export default function Landing() {
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <SearchBar
        onChange={() => console.log("onChange")}
        onRequestSearch={() => console.log("onRequestSearch")}
        style={{
          margin: "0 auto",
          maxWidth: 800
        }}
      />
      <StyledSearchBar />
    </div>
  );
}