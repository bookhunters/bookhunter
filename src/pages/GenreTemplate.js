import React from 'react'
import axios from 'axios';
import {useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

// Add a book like the example below in its own file


const GenreTemplate = (props) => {

    const [video, setVideo] = useState("PUT DEFAULT IMAGE HERE")
    //const [endVideo, setEndVideo] = useState("NEw VIDeo")
    const [name, setName] = useState("PUT DEFAULT TITLE HERE")
    const [author, setAuthor] = useState("PUT DEFAULT AUTHOR HERE")
    const [book, setBook] = useState("PUT DEFAULT GENRE")
    const [desc, setDesc] = useState("description")
    const [profit, setProfit] = useState("profit")
    const location = useLocation()

    useEffect(() => {
        axios.get("https://mediumdb-356314.uc.r.appspot.com/api/genre/" + (location.state.genre_name))
            .then(res => {
                console.log(res)
                const genreInfo = res.data
                setVideo(genreInfo.genre_youtube_link)
                setName(genreInfo.genre_name)
                setAuthor(genreInfo.authors_showup)
                setBook(genreInfo.books_showup)
                setDesc(genreInfo.genre_description)
                setProfit(genreInfo.genre_profitability)
                console.log(res)

            })
            .catch(err => console.log(err))
    }, [])

    var endVideo = video.slice(30)
    var totalVideo = "https://www.youtube.com/embed/" + endVideo

    const navigate = useNavigate();
    return (
        <div style={{textAlign: "center"}}>
            <h1 style={{fontSize: 80}}> {name} </h1> 
            <br></br>
            <h3 style={{textAlign: "center"}}> {desc} </h3>
            <br></br>
            <h3 style={{textAlign: "center", fontSize: 30}}> Notable Authors: {author}</h3>
            <br></br>
            <h4 style={{textAlign: "center", fontSize: 30}}> Books: {book} </h4>
            <br></br>
            <h5 style={{textAlign: "center", fontSize: 30}}> Profitability: {profit} </h5>
            <br></br>
            <iframe src= {totalVideo} allowFullScreen/>     
        </div>
    );
}
 export default GenreTemplate;