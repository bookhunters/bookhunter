import React from 'react';
const Models = () =>{
  return (
    <div>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css"></link>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

      {/*Data table does not currently work*/}

      {/* <script type="text/javascript" language="javascript">
        $(document).ready(function () {
          $('#Books').DataTable();
        });
      </script> */}
      <table id="Books" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Genre</th>
            <th>Publication Date</th>
            <th>Language</th>
            <th>ISBN</th>
            <th>Word Count</th>
            <th>Book Image</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Harry Potter and the Sorcerer's Stone</td>
            <td>JK Rowling</td>
            <td>Fantasy</td>
            <td>June 26, 1997</td>
            <td>English</td>
            <td>9780590353403</td>
            <td>76,944</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>2</td>
            <td>A Study In Scarlet</td>
            <td>Arthur Conan Doyle</td>
            <td>Mystery</td>
            <td>1887</td>
            <td>English</td>
            <td>9780938501329</td>
            <td>43,625</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Pride and Prejudice</td>
            <td>Jane Austen</td>
            <td>Romance</td>
            <td>January 28, 1813</td>
            <td>English</td>
            <td>9780140430721</td>
            <td>122,189</td>
            <td>PLACEHOLDER</td>
          </tr>      
        </tbody>
      </table>
      {/* <table id="Genre" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>ID</th>
            <th>Authors</th>
            <th>Books</th>
            <th>Description</th>
            <th>Video</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>2</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>3</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>      
        </tbody>
      </table> */}
            {/* <table id="Authors" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Origin</th>
            <th>Birth Date</th>
            <th>Genre</th>
            <th>Books</th>
            <th>Languages</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>2</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>
          <tr>
            <td>3</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
            <td>PLACEHOLDER</td>
          </tr>      
        </tbody>
      </table> */}
      <br></br>
      <h1 style={{textAlign: "center"}}>NO DATA TO REPORT YET, MODELS A WORK IN PROGRESS AS WE GET MORE DATA.</h1>
    </div>
  );

}
export default Models;
