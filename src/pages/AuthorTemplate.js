import React from 'react'
import axios from 'axios';
import {useState, useEffect} from 'react'
import BookInfo from '../components/BookInfo'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';


const AuthorTemplate = (props) => {

    const [img, setImg] = useState("PUT DEFAULT IMAGE HERE")
    const [name, setName] = useState("PUT DEFAULT TITLE HERE")
    const [birth, setBirth] = useState("PUT DEFAULT AUTHOR HERE")
    const [genre, setGenre] = useState("PUT DEFAULT GENRE")
    const [books, setBooks] = useState("PUT BOOKS")
    const [education, setEducation] = useState("PUT DEFAULT ISBN")
    const [nationality, setNationality] = useState("Nationality")
    const [desc, setDesc] = useState("PUT DESC HERE")
    const location = useLocation()

    useEffect(() => {
        axios.get("https://mediumdb-356314.uc.r.appspot.com/api/author/" + (location.state.author_name))
            .then(res => {
                console.log(res)
                const authorInfo = res.data
                // var badImage = authorInfo.book_image;
                setImg(authorInfo.author_image)
                setName(authorInfo.author_name)
                setBirth(authorInfo.author_birth_date)
                setGenre(authorInfo.genres_showup)
                setBooks(authorInfo.books_showup)
                setEducation(authorInfo.author_education)
                setNationality(authorInfo.author_nationality)
                setDesc(authorInfo.author_description)
                console.log(res)

            })
            .catch(err => console.log(err))
    }, [])

    const navigate = useNavigate();
    return (
        <div style={{textAlign: "center"}}>
            <h1 style={{textAlign: "center"}}> {name} </h1>
            <div>
                <img src={img} style={{minHeight: 300, minWidth: 300, maxHeight: 400, maxWidth:300}}/>
            </div>
            <br></br>
            <div style={{justifyContent: 'center'}}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableBody>
                        <TableCell>Birth Date: {birth}</TableCell>
                        <TableCell>Nationality: {nationality}</TableCell>
                        <TableCell>Education: {education}</TableCell>                       
                        <TableCell onClick={() => navigate("/genreinfo")}>Genres: {genre}</TableCell>
                        <TableCell onClick={() => navigate("/bookinfo", {state: {book_title : props.books_showup[0]}})}>Books: {books} </TableCell>
                    </TableBody>
                </Table>
            </TableContainer>
            <br></br>
            <div style={{backgroundColor: "white"}}>
                {desc}
            </div>
            </div>
        </div>
    );
}
export default AuthorTemplate;