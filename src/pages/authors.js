import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate';
import AuthorCard from '../components/AuthorCard';
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Pagination from '@mui/material/Pagination';

const Authors = () => {

    const [authorList, setAuthorList] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [currentURL, setCurrentURL] = useState("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=1")
    const [authorSearch, setAuthorSearch] = useState("");
    const [authorSort, setAuthorSort] = useState("");
    const [numItems, setNumItems] = useState(0);
    const [sortMethod, setSortMethod] = useState(0);
    
    useEffect(() => {
        axios.get(currentURL)
            .then(res => {
                console.log(res.data)
                setAuthorList(res.data.map(author => {
                    return(
                        <AuthorCard
                            name = {author.author_name}
                            genres_showup= {author.genres_showup}
                            language= {author.author_nationality}
                            birth_date= {author.author_birth_date}
                            books_showup={author.books_showup}
                            key = {author.author_id}
                        />
                    )}
                ))
                setNumItems(res.data.length)
                console.log(numItems)
                console.log(authorList.length)
          })
        .catch(err => console.log(err))
        urlForPage()
    }, [currentURL])


    function urlForPage() {
        return "https://mediumdb-356314.uc.r.appspot.com/api/authors?page=" + (currentPage + 1)
    }

    const handleChange = (event, value) => {
        setCurrentPage(value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=" + value + "&q=" + authorSearch + "&sort=" + authorSort)
        console.log(currentURL);
    };

    let handleSearch = (e) => {
        setAuthorSearch(e.target.value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=1&q=" + authorSearch);
    }

    function handleSort(sortBy) {
        console.log(sortBy);
        if (sortBy == sortMethod){
            setSortMethod(0);
            setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=1&q=" + authorSearch);
            setCurrentPage(1)
            return;
        }
        let sortString = "";
        switch (sortBy) {
            case 1:
                setSortMethod(1);
                sortString = "name"
                break;
            case 2:
                setSortMethod(2);
                sortString = "books"
                break;
            case 3:
                setSortMethod(3);
                sortString = "genres"
                break;
            case 4:
                setSortMethod(4);
                sortString = "birth_date"
                break;
            case 5:
                setSortMethod(5);
                sortString = "nationality"
                break;
            default:
                break;
        }
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=1&q=" + authorSearch + "&sort=" + sortString);
        setCurrentPage(1)
        setAuthorSort(sortString)
    }

    return(
        <div className="Authors">
            <h1 style= {{fontSize: 50}}>Authors</h1>
            <br></br> 
            <div className="search">
                <TextField
                    id="outlined-basic"
                    onChange={handleSearch}
                    variant="outlined"
                    fullWidth
                    label="Search"
                />
            </div>
            <br></br>
            <div style={{fontSize: 20, textAlign: "center"}}>
                SORT BY:
                <ButtonGroup variant="contained" aria-label="sorting buttons">
                    <Button onClick={() => handleSort(1)}>Name</Button>
                    <Button onClick={() => handleSort(2)}>Books</Button>
                    <Button onClick={() => handleSort(3)}>Genres</Button>
                    <Button onClick={() => handleSort(4)}>Birthdate</Button>
                    <Button onClick={() => handleSort(5)}>Nationality</Button>
                </ButtonGroup>
            </div>
            <br></br>
            {authorList}
            <br></br>
            <Pagination
                count={11}
                page={currentPage}
                onChange={handleChange}
            />
        </div>
    )

}

export default Authors;