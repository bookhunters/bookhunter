import React from 'react'
import BookInfo from '../components/BookInfo'

const Info = () =>{
    return(
        <div style={{justifyContent: 'center'}}>
            <BookInfo
                title='Harry Potter and the Sorcerers Stone'
                author='J.K. Rowling'
                date='June 26, 1997'
                lang='English'
                isbn='9780590353403'
                count='76,944'
            />
        </div>
    );
}

export default Info;