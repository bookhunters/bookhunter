import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate';
import BookCard from '../components/BookCard';
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Pagination from '@mui/material/Pagination';

// This is where you put the books that we are hard coding in
// pass in the information like the harry potter example below

const Books = () => {

    const [bookList, setBookList] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [currentURL, setCurrentURL] = useState("https://mediumdb-356314.uc.r.appspot.com/api/books")
    const [bookSearch, setBookSearch] = useState("");
    const [bookSort, setBookSort] = useState("");
    const [numItems, setNumItems] = useState(0);
    const [sortMethod, setSortMethod] = useState(0);
    
    useEffect(() => {
        axios.get(currentURL)
            .then(res => {
                console.log(res.data)
                setBookList(res.data.map(book => {
                    return(
                        <BookCard
                        author = {book.author_showup}
                        title = {book.book_title}
                        genre= {book.genre_showup}
                        date= {book.book_publication_date}
                        lang= {book.book_language}
                        count= {book.book_page_count}
                        key={book.book_id}
                        id={book.book_id}
                        ratings={book.book_ratings}
                        />
                    )
                }))
                setNumItems(res.data.length)
            })
            .catch(err => console.log(err))
    }, [currentURL])

    const handleChange = (event, value) => {
        setCurrentPage(value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/books?page=" + value + "&q=" + bookSearch + "&sort=" + bookSort)
        console.log(currentURL);
    };

    let handleSearch = (e) => {
        setBookSearch(e.target.value);
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/books?page=1&q=" + bookSearch + "&sort=" + bookSort);
        setCurrentPage(1)
        console.log(currentURL);
    }

    function handleSort(sortBy) {
        console.log(sortBy);
        if (sortBy == sortMethod){
            setSortMethod(0);
            setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/books?page=1&q=" + bookSearch);
            setCurrentPage(1)
            console.log(currentURL);
            return;
        }
        let sortString = "";
        switch (sortBy) {
            case 1:
                setSortMethod(1);
                sortString = "name"
                break;
            case 2:
                setSortMethod(2);
                sortString = "page_count"
                break;
            case 3:
                setSortMethod(3);
                sortString = "ratings"
                break;
            case 4:
                setSortMethod(4);
                sortString = "date"
                break;
            case 5:
                setSortMethod(5);
                sortString = "genre"
                break;
            default:
                break;
        }
        setCurrentURL("https://mediumdb-356314.uc.r.appspot.com/api/books?page=1&q=" + bookSearch + "&sort=" + sortString);
        setCurrentPage(1)
        setBookSort(sortString)
    }

    return(
        <div className="Book">
            <h1 style= {{fontSize: 50}}>Books</h1>
            <br></br> 
            <div className="search">
                <TextField
                    id="outlined-basic"
                    onChange={handleSearch}
                    variant="outlined"
                    fullWidth
                    label="Search"
                />
            </div>
            <br></br>
            <div style={{fontSize: 20, textAlign: "center"}}>
                SORT BY:
                <ButtonGroup variant="contained" aria-label="sorting buttons">
                    <Button onClick={() => handleSort(1)}>Name</Button>
                    <Button onClick={() => handleSort(2)}>Page Count</Button>
                    <Button onClick={() => handleSort(3)}>Ratings</Button>
                    <Button onClick={() => handleSort(4)}>Date</Button>
                    <Button onClick={() => handleSort(5)}>Genre</Button>
                </ButtonGroup>
            </div>
            <br></br>
            {bookList}
            <br></br>
            <div>
                <Pagination
                    count={100}
                    page={currentPage}
                    onChange={handleChange}
                />
            </div>
        </div>
    )
}

export default Books;