import React from 'react'
import axios from 'axios';
import {useState, useEffect} from 'react'
import BookInfo from '../components/BookInfo'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

// Add a book like the example below in its own file


const BookTemplate = (props) => {

    const [img, setImg] = useState("PUT DEFAULT IMAGE HERE")
    const [title, setTitle] = useState("PUT DEFAULT TITLE HERE")
    const [author, setAuthor] = useState("PUT DEFAULT AUTHOR HERE")
    const [genre, setGenre] = useState("PUT DEFAULT GENRE")
    const [publicationDate, setPublicationDate] = useState("PUT DEFAULT DATE")
    const [language, setLanguage] = useState("PUT DEFAULT LANGUAGE")
    const [isbn, setIsbn] = useState("PUT DEFAULT ISBN")
    const [wordcount, setWordCount] = useState("PUT DEFAULT WORD COUNT")
    const [desc, setDesc] = useState("description")
    const location = useLocation()
    const url = "https://mediumdb-356314.uc.r.appspot.com/api/book/" + (location.state.book_title)
    console.log(url);

    useEffect(() => {
        axios.get(url)
            .then(res => {
                console.log(res)
                const bookInfo = res.data
                var badImage = bookInfo.book_image;
                setImg(bookInfo.book_image)
                setTitle(bookInfo.book_title)
                setAuthor(bookInfo.author_showup)
                setGenre(bookInfo.genre_showup)
                setPublicationDate(bookInfo.book_publication_date)
                setLanguage(bookInfo.book_language)
                setIsbn(bookInfo.book_ISBN)
                setWordCount(bookInfo.book_page_count)
                setDesc(bookInfo.book_description)
                console.log(res)

            })
            .catch(err => console.log(err))
    }, [])

    const navigate = useNavigate();
    return (
        <div style={{textAlign: "center"}}>
            <h1 style={{fontSize: 50}}>{title}</h1>
            <img style={{minWidth: 300, minHeight: 300, maxWidth : 400, maxHeight: 400}}src={img}/>
            <br></br>
            <br></br>
            <div style={{justifyContent: 'center'}}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableBody>
                        <TableCell onClick={() => navigate("/authorinfo", {state: {author_name : props.author_showup}})}>Author: {author}</TableCell>
                        <TableCell onClick={() => navigate("/genreinfo")}>Genre: {genre}</TableCell>
                        <TableCell>Publication Date: {publicationDate}</TableCell>
                        <TableCell>Language: {language}</TableCell>
                        <TableCell>ISBN: {isbn}</TableCell>
                        <TableCell>Page Count: {wordcount}</TableCell>
                    </TableBody>
                </Table>
            </TableContainer>
            <br></br>
            <div style={{backgroundColor: "white"}}>
                {desc}
            </div>
            </div>
        </div>
    );
}
export default BookTemplate;