import React from 'react'
import { useNavigate } from 'react-router-dom';


const Romance = () => {
    const navigate = useNavigate();
    return (
        <div style={{textAlign: "center"}}>
            <h1 style={{fontSize: 100}}> Romance </h1> 
            <h2 style={{textAlign: "center"}}> The romance genre is a popular category of books that consistently churns out bestsellers. The aim of the genre is simple, showcasing a love story where two people overcome adversity to obtain their happily ever after. </h2>
            <br></br>
            <h3 style={{textAlign: "center"}}> Notable Authors:</h3>
            <div style={{fontSize: 25}} onClick={() => navigate("/janeausten")}>
                Jane Austen
            </div>
            <br></br>
            <iframe src= "https://www.youtube.com/embed/9b1fXsW5w68" allowFullScreen/>     
        </div>
    );
}
export default Romance;