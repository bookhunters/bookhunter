# *1-1
# Get all authors: backend-355002.appspot.com/api/authors

# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })


# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // test whether the return string contains the author's name 'Chinua Achebe'
# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Fantasy");
#     pm.expect(pm.response.text()).to.include("Yate, United Kingdom");
# });


# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Sir Arthur Conan Doyle");
#     pm.expect(pm.response.text()).to.include("Edinburgh, Midlothian, Scotland");
# });


# * 1-2
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // check when id = 2, whether we can get the correct values
# pm.test("test id = 2 and return value", function() {
#     var jsonData = pm.response.json();
#     pm.expect(pm.response.text()).to.include("Sir Arthur Conan Doyle");
#     pm.expect(pm.response.text()).to.include("May, 22 1859 - July 7, 1930");
#     pm.expect(pm.response.text()).to.include("English");
# });

# * 2-1
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# //test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // test whether the return string contains literary collections book genre
# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Literary Collections");
#     pm.expect(pm.response.text()).to.include("Fiction");
# });

# // test whether the return string contains the author's name 'Chinua Achebe'
# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Chinua Achebe");
#     pm.expect(pm.response.text()).to.include("No Sweetness Here");
# });


# *2-2
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# //test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // check when id = 2, whether we can get the correct values
# pm.test("test id = 2 and return value", function() {
#     var jsonData = pm.response.json();
#     pm.expect(pm.response.text()).to.include("9781578051212");
#     pm.expect(pm.response.text()).to.include("Edward Abbey");
#     pm.expect(pm.response.text()).to.include("2005-07");
# });


# // check when id = 5, whether we can get the correct values
# // pm.test("test id = 5 and return value", function() {
# //     var jsonData = pm.response.json();
# //     pm.expect(pm.response.text()).to.include("The Ruined Map");
# //     pm.expect(pm.response.text()).to.include("Fiction");
# // });

# 2-3
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# //test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# 3-1
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# // test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // test whether the JSON returns the correct content
# // test whether the body string contains book genre 'Performing Arts'
# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Performing Arts");
#     pm.expect(pm.response.text()).to.include("History");
# });

# // test whether the body string contains the book called 'Beyond the Wall'
# pm.test("Body matches string", function () {
#     pm.expect(pm.response.text()).to.include("Beyond the Wall");
#     pm.expect(pm.response.text()).to.include("Home And Exile");
# });

# 3-2
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# // test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });

# // check when id = 2, whether we can get the correct values
# pm.test("test id = 2 and return value", function() {
#     var jsonData = pm.response.json();
#     pm.expect(pm.response.text()).to.include("Biography & Autobiography");
#     pm.expect(pm.response.text()).to.include("Peter Ackroyd");
#     pm.expect(pm.response.text()).to.include("The Life of Thomas More");
# });


# // check when id = 5, whether we can get the correct values
# // pm.test("test id = 5 and return value", function() {
# //     var jsonData = pm.response.json();
# //     pm.expect(pm.response.text()).to.include("Let Us Now Praise Famous Men");
# //     pm.expect(pm.response.text()).to.include("James Agee");
# // });

# 3-3
# //test whether the status is ok and whether the api returns the json format
# pm.test("URL is working", function(){
#     pm.response.to.be.ok;
#     pm.response.to.be.json;
# })

# //test whether the status code is 200
# pm.test("Status code is 200", function () {
#     pm.response.to.have.status(200);
# });


# // check when id = 5, whether we can get the correct values
# pm.test("test number = 5 and return value", function() {
#     var jsonData = pm.response.json();
#     pm.expect(pm.response.text()).to.include("History");
#     pm.expect(pm.response.text()).to.include("Cotton Tenants");
#     pm.expect(pm.response.text()).to.include("Things Fall Apart");
# });

# // check when id = 2, whether we can get the correct values
# // pm.test("test id = 2 and return value", function() {
# //     var jsonData = pm.response.json();
# //     pm.expect(jsonData.id).to.eql(2);
# //     pm.expect(pm.response.text()).to.include("Edward Abbey);
# //     pm.expect(pm.response.text()).to.include("Things Fall Apart");
# // });