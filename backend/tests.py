import app
import unittest
import requests
import json
from unittest import TestCase



class UnitTests(TestCase):
    def test_always_right(self):
        self.assertEqual(1, 1)
        pass

    def test_get_all_books(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/books?page=1")
        self.assertEqual(response.status_code, 200)
        res_json = (response.json())
        print(len(res_json))
        self.assertEqual(len(res_json), 10)
        pass

    def test_get_all_genres(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/genres?page=1")
        self.assertEqual(response.status_code, 200)
        res_json = (response.json())
        self.assertEqual(len(res_json), 10)
        pass

    def test_get_all_authors(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/authors?page=1")
        self.assertEqual(response.status_code, 200)
        res_json = (response.json())
        self.assertEqual(len(res_json), 10)
        pass
    
    def test_get_one_author(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/author/Douglas%20Adams")
        self.assertEqual(response.status_code, 200)
        res_json = response.json()
        self.assertEqual(len(res_json), 9)
        pass
    
    def test_get_one_genre(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/genre/Fiction")
        self.assertEqual(response.status_code, 200)
        res_json = response.json()
        self.assertEqual(len(res_json), 7)
        pass
    
    def test_get_one_book(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/book/Fire%20on%20the%20Mountain")
        self.assertEqual(response.status_code, 200)
        res_json = response.json()
        self.assertEqual(len(res_json), 13)
        pass

    def test_get_one_author2(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/author/Edward")
        self.assertEqual(response.status_code, 404)
        res_json = response.json()
        self.assertNotEqual(len(res_json), 9)
        pass
    
    def test_get_one_genre2(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/genre/fire")
        self.assertEqual(response.status_code, 404)
        res_json = response.json()
        self.assertNotEqual(len(res_json), 7)
        pass
    
    def test_get_one_book2(self):
        response = requests.get("https://mediumdb-356314.uc.r.appspot.com/api/book/Down%20River")
        self.assertEqual(response.status_code, 404)
        res_json = response.json()
        self.assertNotEqual(len(res_json), 13)
        pass



# ## NOT SURE IF WE NEED THE BELOW TESTS -Aditya ##
        
    def test_genre_filter(self):

        pass

    def test_author_filter(self):
        pass

    def test_sort_books(self):
        pass

    def test_sort_genres(self):
        pass

    def test_sort_authors(self):
        pass

    def test_search_books_by_name(self):
        pass
    
    def test_search_books_by_genre(self):
        pass

    def test_search_books_by_author(self):
        pass

    def test_search_books_by_isbn(self):
        pass

if __name__ == "__main__":
    unittest.main()