from operator import and_
from models import app, db, Book, Author, Genre
import flask_sqlalchemy
from sqlalchemy import or_, and_


def search_authors(query, q):
    if q == None:
        return query

    queries = q.split(" ")
    print(queries)
    items = []
    for item in queries:
        items.append(Author.author_name.ilike("%{}%".format(item)))
        items.append(Author.author_birth_date.ilike("%{}%".format(item)))
        items.append(Author.author_nationality.ilike("%{}%".format(item)))
        items.append(Author.author_description.ilike("%{}%".format(item)))
        items.append(Author.author_education.ilike("%{}%".format(item)))
        items.append(Author.books_showup.ilike("%{}%".format(item)))
        items.append(Author.genres_showup.ilike("%{}%".format(item)))
    
    query = query.filter(or_(*tuple(items)))
    return query


def filter_authors(query, name, date):
    if name != None:
        if name == 0:
            query = query.filter(Author.author_name < "F")
        elif name == 1:
            query = query.filter(and_(Author.author_name >= "F",Author.author_name < "L"))
        elif name == 2:
            query = query.filter(and_(Author.author_name >= "L",Author.author_name < "S"))
        else:
            query = query.filter(Author.author_name >= "S")

    if date != None:
        if date == 0:
            query = query.filter(or_(Author.author_birth_date == "N/A", Author.author_birth_date <= '1900-01-01'))
        elif date == 1:
            query = query.filter(and_(Author.author_birth_date.between('1900-01-01', '1950-01-01'), Author.author_birth_date != "N/A"))
        elif date == 2:
            query = query.filter(and_(Author.author_birth_date.between('1950-01-02', '2000-01-01'), Author.author_birth_date != "N/A"))
        else:
            query = query.filter(and_(Author.author_birth_date >='2000-01-02', Author.author_birth_date != "N/A"))
    return query


def sort_authors(sort, query):
    category = None
    if sort == "name":
        category = Author.author_name
    elif sort == "books":
        category = Author.books_showup
    elif sort == "genres":
        category = Author.genres_showup
    elif sort == "birth_date":
        category = Author.author_birth_date
    elif sort == 'nationality':
        category = Author.author_nationality
    else:
        category = Author.author_id
    print(category)
    return query.order_by(category)

