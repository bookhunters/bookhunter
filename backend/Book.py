from models import app, db, Book, Author, Genre
import flask_sqlalchemy
from sqlalchemy import or_, and_


def search_books(query, q):
    if q == None:
        return query

    queries = q.split(" ")
    print(queries)
    items = []
    for item in queries:
        items.append(Book.book_title.ilike("%{}%".format(item)))
        items.append(Book.book_ISBN.ilike("%{}%".format(item)))
        items.append(Book.book_language.ilike("%{}%".format(item)))
        items.append(Book.book_publication_date.ilike("%{}%".format(item)))
        try:
            items.append(Book.book_ratings.in_([int(item)]))
        except ValueError:
            pass
        try:
            items.append(Book.book_page_count.in_([int(item)]))
        except ValueError:
            pass
        items.append(Book.book_description.ilike("%{}%".format(item)))
        items.append(Book.author_showup.ilike("%{}%".format(item)))
        items.append(Book.genre_showup.ilike("%{}%".format(item)))
    query = query.filter(or_(*tuple(items)))
    return query


def filter_books(query, ratings, page_count, date):
    if ratings != None:
        if ratings == 0:
            query = query.filter(Book.book_ratings.between(0, 0.99))
        elif ratings == 1:
            query = query.filter(Book.book_ratings.between(1, 1.99))
        elif ratings == 2:
            query = query.filter(Book.book_ratings.between(2, 2.99))
        elif ratings == 3:
            query = query.filter(Book.book_ratings.between(3, 3.99))
        elif ratings == 4:
            query = query.filter(Book.book_ratings.between(4, 4.99))
        else:
            query = query.filter(Book.book_ratings.between(4.99, 5))
    
    if page_count != None:
        if page_count == 0:
            query = query.filter(Book.book_page_count.between(0, 249))
        elif page_count == 1:
            query = query.filter(Book.book_page_count.between(250, 499))
        elif page_count == 2:
            query = query.filter(Book.book_page_count.between(500, 749))
        elif page_count == 3:
            query = query.filter(Book.book_page_count.between(750, 999))
        else:
            query = query.filter(Book.book_page_count >= 1000)

    if date != None:
        if date == 0:
            query = query.filter(Book.book_publication_date == "N/A")
        elif date == 1:
            query = query.filter(and_(Book.book_publication_date.between('1900-01-01', '1950-01-01'), Book.book_publication_date != "N/A"))
        elif date == 2:
            query = query.filter(and_(Book.book_publication_date.between('1950-01-02', '2000-01-01'), Book.book_publication_date != "N/A"))
        else:
            query = query.filter(and_(Book.book_publication_date >='2000-01-02', Book.book_publication_date != "N/A"))

    return query


def sort_books(sort, query):
    category = None
    if sort == "name":
        category = Book.book_title
    elif sort == "page_count":
        category = Book.book_page_count
    elif sort == "ratings":
        category = Book.book_ratings
    elif sort == "date":
        category = Book.book_publication_date
    elif sort == 'genre':
        category = Book.genre_showup
    elif sort == 'book':
        category = Book.author_showup
    else:
        category = Book.book_id
    if category == Book.book_ratings:
        return query.order_by(category.desc())
    return query.order_by(category)

