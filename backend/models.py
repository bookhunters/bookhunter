from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
CORS(app)
app.debug = True

# Change this accordingly 
USER ="postgres"
PASSWORD ="123456"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="bookhunter_db"


# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine


# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)

# DB table relationships:
#
# book and genre: One book, one genre; One genre, many books : One to many
# book and author: One book, one authors; one author, many books: one to many
# author and genre: One author, many genres; one genre, many authors: many to many

# Data we have now: given author, know genres
#                   given book, know genrel

#build many to many association tables:
link_genre_author =  db.Table('link_genre_author',
   db.Column('genre_id', db.Integer, db.ForeignKey('genre.genre_id')), 
   db.Column('author_id', db.Integer, db.ForeignKey('author.author_id'))
   )

class Book(db.Model):
    __tablename__ = 'book'
    book_id = db.Column(db.Integer, primary_key=True)
    book_title = db.Column(db.String())
    book_ISBN = db.Column(db.String())
    book_language = db.Column(db.String())
    book_publication_date = db.Column(db.String())
    book_page_count = db.Column(db.Integer)
    book_ratings = db.Column(db.Integer)
    book_description = db.Column(db.String())
    book_image = db.Column(db.String())
    genre_id = db.Column(db.Integer, db.ForeignKey('genre.genre_id'))
    genre_showup = db.Column(db.String())
    author_id = db.Column(db.Integer, db.ForeignKey('author.author_id'))
    author_showup = db.Column(db.String())


# Define Genre table/data model
class Genre(db.Model):
    __tablename__ = 'genre'
    genre_id = db.Column(db.Integer, primary_key=True)
    genre_name = db.Column(db.String())
    genre_description = db.Column(db.String())
    genre_youtube_link = db.Column(db.String())
    genre_profitability = db.Column(db.Integer)
    books_showup = db.Column(db.String())
    authors_showup = db.Column(db.String())
    genre_books = db.relationship('Book', backref = 'genre')


# tofix based on collected data
# Define Author table/data model
class Author(db.Model):
    __tablename__ = 'author'
    author_id = db.Column(db.Integer, primary_key=True)
    author_name = db.Column(db.String())
    author_birth_date = db.Column(db.String())
    author_image = db.Column(db.String())
    author_nationality = db.Column(db.String())
    author_description = db.Column(db.String())
    author_education = db.Column(db.String())
    books_showup = db.Column(db.String())
    genres_showup = db.Column(db.String())
    author_genres = db.relationship('Genre', secondary = 'link_genre_author', backref='familiar')
    author_books = db.relationship('Book', backref = 'author')



#db.drop_all()
#db.create_all()