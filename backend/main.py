# from flask import Flask, jsonify;
# from flask_cors import CORS
# app = Flask(__name__)
# CORS(app)
# # data for 3 books
# # books = [{
# #     "id": 1,
# #     "title": "Harry Potter and the Sorcerers Stone",
# #     "author": "J.K. Rowling",
# #     "genre": "Fantasy",
# #     "publication date": "June 26, 1997",
# #     "language": "English",
# #     "isbn": "9780590353403",
# #     "word count": 76944,
# #     "book_image" : "https://covers.openlibrary.org/b/olid/OL22856696M-L.jpg"
# #     },
# #     {    
# #     "id": 2,
# #     "title": "A Study in Scarlet",
# #     "author": "Arthur Conan Doyle",
# #     "genre": "Mysterye",
# #     "publication date": "1887",
# #     "language": "English",
# #     "isbn": "9780938501329",
# #     "word count": 43625,
# #     "book_image" : "https://covers.openlibrary.org/b/id/9248496-L.jpg"
# #     },
# #     {
# #     "id": 3,
# #     "title": "Pride and Prejudice",
# #     "author": "Jane Austen",
# #     "genre": "Romance",
# #     "publication date": "January 28, 1813",
# #     "language": "English",
# #     "isbn": "9780140430721",
# #     "word count": 122189,
# #     "book_image" : "https://covers.openlibrary.org/b/id/12621956-L.jpg"
# #     },
# # ]

# books = [
#   {
#     "id": 1,
#     "authors": ["Edward Abbey"],
#     "genre": "Fiction",
#     "isbn": ["0795317395", "9780795317392"],
#     "language": "en",
#     "publication_date": "2011-08-21",
#     "title": "Fire on the Mountain",
#     "page_count": 192,
#     "ratings": 4,
#     "description": "A New Mexico man faces off against the government in a battle over his land in this novel by the author of Desert Solitaire. \u00a0 After nine months away at school, Billy Vogelin Starr returns home to his beloved New Mexico\u2014only to find his grandfather in a standoff with the US government, which wants to take his land and turn it into an extension of the White Sands Missile Range. \u00a0 Facing the combined powers of the US county sheriff, the Department of the Interior, the Atomic Energy Commission, and the US Air Force, John Vogelin stands his ground\u2014because to Vogelin, his land is his life. When backed into a corner, a tough old man like him will come out fighting\u00a0.\u00a0.\u00a0. \u00a0Fire on the Mountain is a suspenseful page-turner by \u201cone of the very best writers to deal with the American West\u201d\u2014the acclaimed author of such classics as The Monkey Wrench Gang and the memoir Desert Solitaire (The Washington Post). \u00a0 \u201cAbbey is a fresh breath from the farther reaches and canyons of the diminishing frontier.\u201d \u2014Houston Chronicle \u00a0 \u201cThe Thoreau of the American West.\u201d \u2014Larry McMurtry, Pulitzer Prize\u2013winning author of Lonesome Dove",
#     "book_image": "http://books.google.com/books/publisher/content?id=qXgqAAAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70az0m2yhWCZbFh7KibSRvkwwBXpGPozug4iZwnnrlEdyfeX3jcu0u0C8--x2_DPvWwWoNw7rvOZW4Iv1mvUxO8dtlz9BiKgaghz1yxLpkfR242oCfGqLv9gFfpMbVnAa0nbHFc&source=gbs_api"
#   },
#   {
#     "id": 2,
#     "authors": ["Edward Abbey"],
#     "genre": "Fiction",
#     "isbn": ["1578051215", "9781578051212"],
#     "language": "en",
#     "publication_date": "2005-07",
#     "title": "The Best of Edward Abbey",
#     "page_count": 433,
#     "ratings": 3,
#     "description": "In 1984, the late great Edward Abbey compiled this reader, endeavoring, as he says in his preface, \"to present what I think is both the best and most representative of my writing--so far.\" Two decades later, it remains the only major collection of his work chosen by Abbey himself, a rich feast of fiction and prose by the singular American writer whom Larry McMurtry called \"the Thoreau of the American West\" and whom Alice Hoffman hailed as \"the voice of all that is ornery and honorable.\" Devoted Abbey fans along with readers just discovering his work will find a mother lode of treasures here: generous chunks of his best novels, including The Brave Cowboy, Black Sun, and his classic The Monkey Wrench Gang; and more than a score of his evocative, passionate, trenchant essays--a genre in which he produced acknowledged masterpieces such as Desert Solitaire. There is even an excerpt from a novel he was working on in 1984, eventually published as The Fool's Progress. Scattered throughout are the author's own petroglyph-style sketches. Abbey went on publishing new work until his untimely death in 1989 at age sixty, so this new edition includes a selection of later Abbey: a chapter from Hayduke Lives!, the hilarious sequel to The Monkey Wrench Gang; excerpts from his revealing journals; a little-known account of a trip to the Sea of Cortez; and examples of his poetry. A new foreword by Doug Peacock --Abbey's close friend and the model for the flamboyant activist Hayduke--offers a fond appreciation of this largerthan- life figure in American letters.",
#     "book_image": "http://books.google.com/books/content?id=W76wAAAAIAAJ&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE731KAI8gaEXCrodT7USogGeVeDqYHNekP6WhJnoEmcbo2AmcfZUVDBOlbuLQRpY0QnJuEZk--B6CHBUuBONpuQtRsBnXVPHvkIgWkhcE2fw1Ybo8Wf8wbqPvpidzYfUlaI2ZQo1&source=gbs_api"
#   },
#   {
#     "id": 3,
#     "authors": ["Edward Abbey"],
#     "genre": "Nature & Travel & Photography",
#     "isbn": ["0805008209", "9780805008203"],
#     "language": "en",
#     "publication_date": "1984-04-15",
#     "title": "Beyond the Wall",
#     "page_count": 224,
#     "ratings": 4,
#     "description": "In this wise and lyrical book about landscapes of the desert and the mind, Edward Abbey guides us beyond the wall of the city and asphalt belting of superhighways to special pockets of wilderness that stretch from the interior of Alaska to the dry lands of Mexico.",
#     "book_image": "http://books.google.com/books/content?id=O1oWR53wZ2IC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE72DssYWfL6eVdMRnIQ07qL_wBSrlfFot54kK2fgXV-bSJaQnr0RxyZP18qRQvap4yN9wleearba5wwbHEVvJAhSCGV5mAvIM-NQ6l7N1W_2tACaaPmExJ2gys2R4TpqrkEykDfi&source=gbs_api"
#   },
#   {
#     "id": 4,
#     "authors": ["Edward Abbey"],
#     "genre": "Nature & Travel & Photography",
#     "isbn": ["0452265630", "9780452265639"],
#     "language": "en",
#     "publication_date": "1991-01-30",
#     "title": "Down the River",
#     "page_count": 256,
#     "ratings": 0,
#     "description": "Down the River is a collection of essays both timeless and timely. It is an exploration of the abiding beauty of some of the last great stretches of American wilderness on voyages down rivers where the body and mind float free, and the grandeur of nature gives rise to meditations on everything from the life of Henry David Thoreau to the militarization of the open range. At the same time, it is an impassioned condemnation of what is being done to our natural heritage in the name of progress, profit, and security. Filled with fiery dawns, wild and shining rivers, and radiant sandstone canyons, it is charged as well with heartfelt, rampageous rage at human greed, blindness, and folly. It is, in short, Edward Abbey at his best, where and when we need him most.",
#     "book_image": "http://books.google.com/books/publisher/content?id=7p9PEAAAQBAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE71-YFgFuxhy3gi8i0HdtmsaGSYGVJVxCrBqQWXfZVfk3qv4ArykAz5hI5O5pGuF2jaBbH5klVE9N8-D1Gh99dgVnqTB9hLlgqA2eySlfuCuXMC7q5aa0bzTjGbF1yoEwNFN92wn&source=gbs_api"
#   },
#   {
#     "id": 5,
#     "authors": ["K\u014db\u014d Abe"],
#     "genre": "Fiction",
#     "isbn": ["0375726527", "9780375726521"],
#     "language": "en",
#     "publication_date": "2001",
#     "title": "The Ruined Map",
#     "page_count": 299,
#     "ratings": 3.5,
#     "description": "Mr. Nemuro, a respected salesman, disappeared over half a year ago, but only now does his alluring yet alcoholic wife hire a private eye. The nameless detective has but two clues - a photo and a matchbook. With these he embarks upon an ever more puzzling",
#     "book_image": "http://books.google.com/books/content?id=M0JfY4yHT24C&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE70mI5gloKjEulB1WVO-1_mFtK8cvqD1jLIASUcHuuYLqq8Wwa57DrR5TYsWc3HtWpqVWQCqsgbS3CSAhtbaX_LIgcgDYM-3xdUzAJSLt4QoZ73oc0RdCcAxIttWRrhDXXYMFJIn&source=gbs_api"
#   },
#   {
#     "id": 6,
#     "authors": ["K\u014db\u014d Abe"],
#     "genre": "Fiction",
#     "isbn": ["0375726519", "9780375726514"],
#     "language": "en",
#     "publication_date": "2001",
#     "title": "The Box Man",
#     "page_count": 178,
#     "ratings": 3,
#     "description": "Kobo Abe, the internationally acclaimed author of Woman in the Dunes, combines wildly imaginative fantasies and naturalistic prose to create narratives reminiscent of the work of Kafka and Beckett.In this eerie and evocative masterpiece, the nameless protagonist gives up his identity and the trappings of a normal life to live in a large cardboard box he wears over his head. Wandering the streets of Tokyo and scribbling madly on the interior walls of his box, he describes the world outside as he sees or perhaps imagines it, a tenuous reality that seems to include a mysterious rifleman determined to shoot him, a seductive young nurse, and a doctor who wants to become a box man himself. The Box Man is a marvel of sheer originality and a bizarrely fascinating fable about the very nature of identity.Translated from the Japanese by E. Dale Saunders.",
#     "book_image": "http://books.google.com/books/content?id=rZKaDWx3UxUC&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE70tZx0-UcsbUH87quJa4_dqtq0-tF1Lb1MVyopXyybRGtqOdqxedccEyVDvsG2qybBYVuBWjpyv7PO8-ugMALurLTSF1H2gful-rW3t6jCNeR8VUzC_sBaE45ZXI1BBTdVUv4Wc&source=gbs_api"
#   },
#   {
#     "id": 7,
#     "authors": ["Chinua Achebe"],
#     "genre": "Fiction",
#     "isbn": ["0141393963", "9780141393964"],
#     "language": "en",
#     "publication_date": "2013-04-25",
#     "title": "Things Fall Apart",
#     "page_count": 176,
#     "ratings": 3.5,
#     "description": "One of the BBC's '100 Novels That Shaped Our World'A worldwide bestseller and the first part of Achebe's African Trilogy, Things Fall Apart is the compelling story of one man's battle to protect his community against the forces of changeOkonkwo is the greatest wrestler and warrior alive, and his fame spreads throughout West Africa like a bush-fire in the harmattan. But when he accidentally kills a clansman, things begin to fall apart. Then Okonkwo returns from exile to find missionaries and colonial governors have arrived in the village. With his world thrown radically off-balance he can only hurtle towards tragedy. First published in 1958, Chinua Achebe's stark, coolly ironic novel reshaped both African and world literature, and has sold over ten million copies in forty-five languages. This arresting parable of a proud but powerless man witnessing the ruin of his people begins Achebe's landmark trilogy of works chronicling the fate of one African community, continued in Arrow of God and No Longer at Ease.'His courage and generosity are made manifest in the work' Toni Morrison'The writer in whose company the prison walls fell down' Nelson Mandela'A great book, that bespeaks a great, brave, kind, human spirit' John UpdikeWith an Introduction by Biyi Bandele",
#     "book_image": "http://books.google.com/books/content?id=tSbWMu_-D5AC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE736W9gTYOZ51kaW0XWsKtDCqpg_b_lk0bOUZEe4KwMHwWT6w9TZ9tBBGCY08EQCAycMRdixqDvGTRli-viwJXcvbDiSsoQ6WnPpwa82KBkI4z_1pnTUfPljR9RtPprStnlNEPRu&source=gbs_api"
#   },
#   {
#     "id": 8,
#     "authors": ["Chinua Achebe"],
#     "genre": "Literary Collections",
#     "isbn": ["0385721331", "9780385721332"],
#     "language": "en",
#     "publication_date": "2001-09-18",
#     "title": "Home And Exile",
#     "page_count": 115,
#     "ratings": 4,
#     "description": "In three powerful essays, the acclaimed, Nigerian-born novelist and author of Things Fall Apart explores the complexities of African culture and discusses the devastating impact of European cultural imperialism on the African experience. Reprint. 25,000 first printing.",
#     "book_image": "http://books.google.com/books/content?id=HyyrBLggpM4C&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE73sH9ebMTj4iXgBCy7U5wpGNgk9c6OAuI8wRHOBHVz5YPioFkrgBCH56dBjqKom8wViBWxMNH7mER6sTCFoYJUGqhAs7gM7yo9FGWNZRpLXN4wRR3rgARrvFiIBSoL9sm12Ljto&source=gbs_api"
#   },
#   {
#     "id": 9,
#     "authors": ["Chinua Achebe"],
#     "genre": "Fiction",
#     "isbn": ["0143131346", "9780143131342"],
#     "language": "en",
#     "publication_date": "2017-05-02",
#     "title": "The African Trilogy",
#     "page_count": 576,
#     "ratings": 5,
#     "description": "Chinua Achebe is considered the father of modern African literature, the writer who \"opened the magic casements of African fiction.\" The African Trilogy--comprised of Things Fall Apart, Arrow of God, and No Longer at Ease--is his magnum opus. In these masterly novels, Achebe brilliantly imagines the lives of three generations of an African community as their world is upended by the forces of colonialism from the first arrival of the British to the waning days of empire.The trilogy opens with the groundbreaking Things Fall Apart, the tale of Okonkwo, a hero in his village, whose clashes with missionaries--coupled with his own tragic pride--lead to his fall from grace. Arrow of God takes up the ongoing conflict between continuity and change as Ezeulu, the headstrong chief priest, finds his authority is under threat from rivals and colonial functionaries. But he believes himself to be untouchable and is determined to lead his people, even if it is towards their own destruction. Finally, in No Longer at Ease, Okonkwo's grandson, educated in England, returns to a civil-service job in Lagos, only to see his morality erode as he clings to his membership in the ruling elite.\u00a0Drawing on the traditional Igbo tales of Achebe's youth, The African Trilogy is a literary landmark, a mythic and universal tale of modern Africa. As Toni Morrison wrote, \"African literature is incomplete and unthinkable without the works of Chinua Achebe. For passion, intellect and crystalline prose, he is unsurpassed.\"",
#     "book_image": "http://books.google.com/books/publisher/content?id=BJhPEAAAQBAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE719TzvdC70qmr5lmd4ywszGvTyskVMepKIGkHmcDqarQudnolBVxfUZItEioANNT-jLB4czDEV8yNzEdugmXv3BW7vzO7hacfCUr2pVRv4j7qLw473az0r4e0L7mtJNVudimC6k&source=gbs_api"
#   },
#   {
#     "id": 10,
#     "authors": ["Chinua Achebe"],
#     "genre": "Literary Collections",
#     "isbn": ["030781646X", "9780307816467"],
#     "language": "en",
#     "publication_date": "2012-02-22",
#     "title": "Hopes and Impediments",
#     "page_count": 208,
#     "ratings": 5,
#     "description": "One of the most provocative and original voices in contemporary literature, Chinua Achebe here considers the place of literature and art in our society in a collection of essays spanning his best writing and lectures from the last twenty-three years. For Achebe, overcoming goes hand in hand with eradicating the destructive effects of racism and injustice in Western society. He reveals the impediments that still stand in the way of open, equal dialogue between Africans and Europeans, between blacks and whites, but also instills us with hope that they will soon be overcome.",
#     "book_image": "http://books.google.com/books/content?id=8rN5yIFTUWkC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70FGhSboVjW_1Lu2hPYBV3oP8JSSEvRRZYTlaNufK2VlQPA8sVT64LQBHnaTHzkY0IwzgzQkHF7fW-YAFyu5Cq2TUwWQVhz4uekTkDyfYd-7nIn6iaOqWeb1Fuss2PgVPLt7nkU&source=gbs_api"
#   },
#   {
#     "id": 11,
#     "authors": ["Kathy Acker"],
#     "genre": "Fiction",
#     "isbn": ["0802134033", "9780802134035"],
#     "language": "en",
#     "publication_date": "1994",
#     "title": "My Mother",
#     "page_count": 268,
#     "ratings": 0,
#     "description": "Based loosely on the relationship between Colette Peignot and Georges Bataille, My Mother: Demonology is the powerful story of a woman's struggle with the contradictory impulses for love and solitude. At the dawn of her adult life, Laure becomes involved in a passionate and all-consuming love affair with her companion, B. But this ultimately leaves her dissatisfied, as she acknowledges her need to establish an identity independent of her relationship with him. Yearning to better understand herself, Laure embarks on a journey of self-discovery, an odyssey that takes her into the territory of her past, into memories and fantasies of childhood, into wildness and witchcraft, into a world where the power of dreams can transcend the legacies of the past and confront the dilemmas of the present. With a poet's attention to the power of language and a keen sense of the dislocation that can occur when the narrative encompasses violence and pornography, as well as the traumas of childhood memory, Kathy Acker here takes another major step toward establishing her vision of a new literary aesthetic.",
#     "book_image": "http://books.google.com/books/content?id=FTzNbM13YYsC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE73B3l7NmKKScw9x0yNoZdsttBslYkA6Qv8c6lA-jAfUmvgS6SMEYukGhfUgvKPTfQSQG8EzyTn1Xk_vCW51I5tiYqRmrW26zZf20N0Ak7Wv50tnrs1H_Acryw_1nLnDRlbhBa9c&source=gbs_api"
#   },
#   {
#     "id": 12,
#     "authors": ["Kathy Acker", "Chris Kraus"],
#     "genre": "Fiction",
#     "isbn": ["0802127622", "9780802127624"],
#     "language": "en",
#     "publication_date": "2017",
#     "title": "Blood and Guts in High School",
#     "page_count": 141,
#     "ratings": 1.5,
#     "description": "A masterpiece of surrealist fiction, steeped in controversy upon its first publication in 1984, Blood and Guts in High School is the book that established Kathy Acker as the preeminent voice of post-punk feminism. With 2017 marking the 70th anniversary of her birth, as well as the 10th year since her death this transgressive work of philosophical, political, and sexual insight--with a new introduction by Chris Kraus--continues to become more relevant than ever before.  In the Mexican city of Merida, ten-year-old Janey lives with Johnny--her \"boyfriend, brother, sister, money, amusement, and father\"--until he leaves her for another woman. Bereft, Janey travels to New York City, plunging into an underworld of gangs and prostitution. After escaping imprisonment, she flees to Tangiers where she meets Jean Genet, and they begin a torrid affair that will lead Janey to her demise. Fantastical, sensual, and fearlessly radical, this hallucinatory collage is both a comic and tragic portrait of erotic awakening.",
#     "book_image": "http://books.google.com/books/content?id=qxjEswEACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE70C7tI7-B1_MgofaSR0yu6K8rpLzWbCfXppgMUCmDQIr_iQ3RZaiSDmGvvSroym48u7xgNA2aWhM3ogmcMuV4PV2oBaCgpozO3RROmWWxSC0dLC5qTQBHSm-9guK3Wf0spwZ5WK&source=gbs_api"
#   },
#   {
#     "id": 13,
#     "authors": ["Forrest J. Ackerman"],
#     "genre": "Performing Arts",
#     "isbn": ["091113705X", "9780911137057"],
#     "language": "en",
#     "publication_date": "1986",
#     "title": "Forrest J. Ackerman, Famous Monster of Filmland",
#     "page_count": 151,
#     "ratings": 0,
#     "description": "N/A",
#     "book_image": "N/A"
#   },
#   {
#     "id": 14,
#     "authors": ["Forrest J. Ackerman"],
#     "genre": "Fiction",
#     "isbn": ["0918736250", "9780918736253"],
#     "language": "en",
#     "publication_date": "2000",
#     "title": "Ackermanthology",
#     "page_count": 308,
#     "ratings": 0,
#     "description": "When a man has read, written, edited, and lived science fiction for a full 70 years, as Forrest J Ackerman has, his collection of sci-fi shorts and semi-shorts is certain to contribute to the pleasure of the connoisseur, and more than delight the relative newcomer to modern literature's hottest field. Herein are timeless tales of aliens, robots, spacemen, future societies, extraordinary experiments, intergalactic love, dinosaurs, and other dimensions -- written with the humor, the horror, the suspense, and the denouements developed by the undisputed geniuses of the twist in the tale, from Bradbury, Wells, and Asimov to lesser-known masters of the trade. Ackerman's following is legion, and readers worldwide, old and new, will revel in every extraordinary page. Book jacket.",
#     "book_image": "http://books.google.com/books/content?id=_CoBAAAACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE72ubGJ_3BgJ-LdQpbVdpU-62pRFee20ughKcJCG7zmN69-1P-c2fCyBrHlTqnICn_XM4Z1OyT74_jdHdCweBJ_d7IYRG8NHljxSAVmpKiKtvj_LRd8rVpCyzevGGIm9CdQimCMW&source=gbs_api"
#   },
#   {
#     "id": 15,
#     "authors": ["Forrest J. Ackerman"],
#     "genre": "Fiction",
#     "isbn": ["188805493X", "9781888054934"],
#     "language": "en",
#     "publication_date": "2004",
#     "title": "Worlds of Tomorrow",
#     "page_count": 176,
#     "ratings": 0,
#     "description": "From deep in the heart of imagination, where galaxies grow, robots rule, and Martians cause mayhem, comes Worlds of Tomorrow: The Amazing Universe of Science Fiction Art. Teeming with gigantic insects, spaceships, and scantily clad heroines, the science fiction pulp and paperback covers of the 1920s through the 1960s represented a generation's vision of the future. New military technology and increased information about space travel fuelled the minds of artists and writers to new heights. Predictions of planetary doom stood side-by-side with visions of Utopia on bookshelves and magazine racks worldwide. Written by lifetime science fiction collector, fan, and B-Movie icon Forrest Ackerman, more than 300 beautifully displayed science fiction covers come back to life in text and chapters grouped by theme. Explore the creative geniuses that moulded our vision of the great unknown into what it is today.",
#     "book_image": "http://books.google.com/books/content?id=pnqYAfxTbu4C&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE70NCSas9vCOUVfcQP101wo5fUqh6O0La6ZBgaDvKpGdmsQEGjrmkN7VZN8Y00hy4tIM6EuVv-0g-LQPnayxk6cCWZw45onO6BG9V7_8MaCNQg2LnQ2Y_xFs-r5uBmprhKzcbtFC&source=gbs_api"
#   },
#   {
#     "id": 16,
#     "authors": ["Forrest J. Ackerman"],
#     "genre": "Performing Arts",
#     "isbn": ["0918736390", "9780918736390"],
#     "language": "en",
#     "publication_date": "2002",
#     "title": "Lon of 1000 Faces!",
#     "page_count": 300,
#     "ratings": 0,
#     "description": "The career of actor Lon Chaney Sr, the Man of 1000 faces. A chronolocical pictorial history featuring stunning characters in hundreds of pictures from his many films. This seminal appreciatio contains biographical sketches and remembrances of Chaney's film and life by Forrest J Ackerman as well as other friends, fans, and relatives. A must for anyone interested in Chaney's films, special effects, makeup artistry, or the history of the Golden Era of Hollywood silent film making.",
#     "book_image": "http://books.google.com/books/content?id=VdQMAAAACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE72VqydO2f8LfK-jA0xQHi1f0_03gUnzU2O0Mhx7rtRhYzeaEYVppNnTR4P0qpKQlpxn15gS1OjE3GrqoLA9hjNU8YeZrB4GfEdgcBIi32mwtbw6cfJiWlIX41940hHBg7EBZqOD&source=gbs_api"
#   },
#   {
#     "id": 17,
#     "authors": ["Peter Ackroyd"],
#     "genre": "Biography & Autobiography",
#     "isbn": ["0099570386", "9780099570387"],
#     "language": "en",
#     "publication_date": "2012",
#     "title": "London",
#     "page_count": 688,
#     "ratings": 0,
#     "description": "An abridged edition of Peter Ackroyd's magisterial biography of the city of London. Prize-winning historian, novelist and broadcaster, Peter Ackroyd takes us on a journey - historical, geographical and imaginative - through the city of London. Moving back and forth through time, Ackroyd is an effortless, exuberant guide to times of plague and pestilence, fire and floods, crime and punishment, and sex and theatre. He brings the ever changing streets alive for the reader and shows us what lies beneath our feet and above our heads. His biography is as rich in detail and fizzing with vitality as the city itself.",
#     "book_image": "http://books.google.com/books/content?id=PnptozHtSA8C&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE738CQU-BFowesl7oIp0pGoXx2ZsLCFAx4ks1h_CrsRagVGOP-o4ep7dxTbN8rt5bSaFLaMDVgVmAE5nIqRzSWDACyeNlOjU2o3phzH72ulKJn3beqv0HskigWZXRtcDAcFyIRAC&source=gbs_api"
#   },
#   {
#     "id": 18,
#     "authors": ["Peter Ackroyd"],
#     "genre": "Biography & Autobiography",
#     "isbn": ["0307823016", "9780307823014"],
#     "language": "en",
#     "publication_date": "2012-06-27",
#     "title": "The Life of Thomas More",
#     "page_count": 480,
#     "ratings": 4,
#     "description": "Peter Ackroyd's The Life of Thomas More is a masterful reconstruction of the life and imagination of one of the most remarkable figures of history. Thomas More (1478-1535) was a renowned statesman; the author of a political fantasy that\u00a0\u00a0gave a name to a literary genre and a worldview (Utopia); and, most famously, a Catholic martyr and saint.Born into the professional classes, Thomas More applied his formidable intellect and well-placed connections to become the most powerful man in England, second only to the king. As much a work of history as a biography, The Life of Thomas More gives an unmatched portrait of the everyday, religious, and intellectual life of the early sixteenth century. In Ackroyd's hands, this renowned \"man for all seasons\" emerges in the fullness of his complex humanity; we see the unexpected side of his character--such as his preference for bawdy humor--as well as his indisputable moral courage.",
#     "book_image": "http://books.google.com/books/content?id=8J9uNOydymUC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE7395IL44MbTU8GUUWzdJHAw1wmvEROo6wgtCcFHkQqFGnodPfUFxtnYBClifHEqZFEiFUZf_rCBJ2ps_ByGc2GpVZyTeAog2ERkCrhKLk3q6duyMkZ-erm6ejAVLjM02atBPTpv&source=gbs_api"
#   },
#   {
#     "id": 19,
#     "authors": ["Thomas Malory"],
#     "genre": "Fiction",
#     "isbn": ["0143106953", "9780143106951"],
#     "language": "en",
#     "publication_date": "2012-10-30",
#     "title": "The Death of King Arthur",
#     "page_count": 336,
#     "ratings": 4,
#     "description": " Acclaimed biographer Peter Ackroyd vibrantly resurrects the legendary epic of Camelot in this modern adaptationThe names of Arthur, Merlin, Lancelot, Guinevere, Galahad, the sword of Excalibur, and the court of Camelot are as recognizable as any from the world of myth. Although many versions exist of the stories of King Arthur and the Knights of the Round Table, Le Morte d'Arthur by Sir Thomas Malory endures as the most moving and richly inventive.In this abridged retelling the inimitable Peter Ackroyd transforms Malory's fifteenth-century work into a dramatic modern story, vividly bringing to life a world of courage and chivalry, magic, and majesty. The golden age of Camelot, the perilous search for the Holy Grail, the love of Guinevere and Lancelot, and the treachery of Arthur's son Mordred are all rendered into contemporary prose with Ackroyd's characteristic charm and panache. Just as he did with his fresh new version of Chaucer's The Canterbury Tales, Ackroyd now brings one of the cornerstones of English literature to a whole new audience.For more than seventy years, Penguin has been the leading publisher of classic literature in the English-speaking world. With more than 1,700\u00a0titles, Penguin Classics represents a global bookshelf of the best works throughout history and across genres and disciplines. Readers trust the\u00a0series to provide authoritative texts enhanced by introductions and notes by distinguished scholars and contemporary authors, as well as up-to-date\u00a0translations by award-winning translators.",
#     "book_image": "http://books.google.com/books/publisher/content?id=il2JDQAAQBAJ&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE72L0v7zLsB4xe-wNLi1MLKzGK86gRllDvl00FaYmnnZBj3F4L9k0hZi3sNIqXy3DK-Cdp6_JP1-Kjw43KuCMhGV-YKSIYZDODWPcriah3d3p_1iFsuuX3Tp9tl5l-KyVK-Ge7be&source=gbs_api"
#   },
#   {
#     "id": 20,
#     "authors": ["Peter Ackroyd", "Geoffrey Chaucer"],
#     "genre": "Fiction",
#     "isbn": ["0670021229", "9780670021222"],
#     "language": "en",
#     "publication_date": "2009",
#     "title": "The Canterbury Tales",
#     "page_count": 436,
#     "ratings": 0,
#     "description": "A fresh, modern prose retelling captures the vigorous and bawdy spirit of Chaucer's classic Renowned critic, historian, and biographer Peter Ackroyd takes on what is arguably the greatest poem in the English language and presents the work in a prose vernacular that makes it accessible to modern readers while preserving the spirit of the original. A mirror for medieval society, Chaucer's Canterbury Tales concerns a motley group of pilgrims who meet in a London inn on their way to Canterbury and agree to take part in a storytelling competition. Ranging from comedy to tragedy, pious sermon to ribald farce, heroic adventure to passionate romance, the tales serve not only as a summation of the sensibility of the Middle Ages but as a representation of the drama of the human condition. Ackroyd's contemporary prose emphasizes the humanity of these characters\u2014as well as explicitly rendering the naughty good humor of the writer whose comedy influenced Fielding and Dickens\u2014yet still masterfully evokes the euphonies and harmonies of Chaucer's verse. This retelling is sure to delight modern readers and bring a new appreciation to those already familiar with the classic tales.  @AprilFools  Oh and the Wyfe of Bathe. Talk about a woman who likes to be perced to the roote.  From Twitterature: The World's Greatest Books in Twenty Tweets or Less",
#     "book_image": "http://books.google.com/books/content?id=C-hymAEACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE71tyjLBUhWVBj--hDMbaDw2SZovRZnNxJfxYeC5bETrt1bjKPgbZdeJ-sfELA7pK5TI0LnoA7F_Wv_56bv9L4p79M2miT8zHYhJE3MJZct4dLPBmLPujqdgbpGtVsPlyM4mUCgu&source=gbs_api"
#   },
#   {
#     "id": 21,
#     "authors": ["Douglas Adams"],
#     "genre": "Fiction",
#     "isbn": ["1476783004", "9781476783000"],
#     "language": "en",
#     "publication_date": "2014-10-07",
#     "title": "The Long Dark Tea-Time of the Soul",
#     "page_count": 256,
#     "ratings": 0,
#     "description": "Beloved, bumbling Detective Dirk Gently returns in this standalone novel\u2014in trade paperback for the first time\u2014from Douglas Adams, the legendary author of one of the most beloved science fiction novels of all time, The Hitchhiker's Guide to the Galaxy.When an explosion goes off at the passenger check-in desk at London's Heathrow Airport, the unexplainable event is deemed an act of God. For private investigator Dirk Gently, it's his job to find out which god would do such a strange thing. In the meantime, one of his clients is murdered and his battle with the cleaning lady over his unbelievably filthy refrigerator comes to a standoff. Is it all connected? Or is this just another stretch of coincidences in the life of off-kilter super-sleuth Dirk Gently? The follow-up to Dirk Gently's Holistic Detective Agency, The Long Dark Tea-Time of the Soul is an unforgettable novel of inimitable wit, humor, and limitless imagination.",
#     "book_image": "http://books.google.com/books/publisher/content?id=u1mpBAAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE71mB1M5G558CvPVz7JDIcac55PqTjNJkh3HiWvL2trsskFziQudDtW3ge8vc7ncnHFTAyOZaUTRcGmgl_G7QbbwITpzUQgaY-vrhsLDmcixCBVhwRCRAG-actmzPlyXmlVHq78N&source=gbs_api"
#   },
#   {
#     "id": 22,
#     "authors": ["Douglas Adams"],
#     "genre": "Fiction",
#     "isbn": ["0593359445", "9780593359440"],
#     "language": "en",
#     "publication_date": "2021",
#     "title": "The Hitchhiker's Guide to the Galaxy: The Illustrated Edition",
#     "page_count": 320,
#     "ratings": "0",
#     "description": "This beautifully illustrated edition of the New York Times bestselling classic celebrates the 42nd anniversary of the original publication--with all-new art by award-winning illustrator Chris Riddell.  SOON TO BE A HULU SERIES * \"An astonishing comic writer.\"--Neil Gaiman  Nominated as one of America's best-loved novels by PBS's The Great American Read  It's an ordinary Thursday morning for Arthur Dent . . . until his house gets demolished. The Earth follows shortly after to make way for a new hyperspace express route, and Arthur's best friend has just announced that he's an alien.  After that, things get much, much worse.  With just a towel, a small yellow fish, and a book, Arthur has to navigate through a very hostile universe in the company of a gang of unreliable aliens. Luckily the fish is quite good at languages. And the book is The Hitchhiker's Guide to the Galaxy . . . which helpfully has the words DON'T PANIC inscribed in large, friendly letters on its cover.  Douglas Adams's mega-selling pop-culture classic sends logic into orbit, plays havoc with both time and physics, offers up pithy commentary on such things as ballpoint pens, potted plants, and digital watches . . . and, most important, reveals the ultimate answer to life, the universe, and everything.  Now, if you could only figure out the question. . . .",
#     "book_image": "http://books.google.com/books/content?id=GMkzzgEACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE71mg50QG94wbsbLtxKllG0AApjpvNGj-l8P4qU85lUacJHMOGxchj3U9gtepTKxFiY-k9c3J5ylL5wcE8gTYCPwQk58X-JFa38zhZK1gCZ4X-fvDB5FZ4wMt0O25njod9uqwWf1&source=gbs_api"
#   },
#   {
#     "id": 23,
#     "authors": ["Douglas Adams", "John Lloyd"],
#     "genre": "Humor",
#     "isbn": ["0330322206", "9780330322201"],
#     "language": "en",
#     "publication_date": "1992",
#     "title": "The Deeper Meaning of Liff",
#     "page_count": 146,
#     "ratings": "0",
#     "description": "The updated, revised edition of \"The Meaning of Liff\", with illustrations from \"Private Eye\" cartoonist Bert Kitchen.",
#     "book_image": "http://books.google.com/books/content?id=ToSEDc13mloC&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE70hUpB9oaWLjjngrW2aVpGPfzVVK3GHEzMnJD8rbCqsPR0VlrEM2IxL2plUacGtmnVc5Ip2qOFhVVVUAJZHkdwMbVaVdpg6uEiRP1Ib57fA5lTnVBjl3o3GwMMsthQc-wOXde7X&source=gbs_api"
#   },
#   {
#     "id": 24,
#     "authors": ["Douglas Adams", "John Lloyd"],
#     "genre": "Humor",
#     "isbn": ["0752227599", "9780752227597"],
#     "language": "en",
#     "publication_date": "2013-10-10",
#     "title": "The Meaning of Liff",
#     "page_count": 224,
#     "ratings": 0,
#     "description": "Special 30th Anniversary edition of Douglas Adams and John Lloyd's classic humour book, revised and updated and including The Deeper Meaning of Liff The Meaning of Liff has sold hundreds of thousands of copies since it was first published in 1983, and remains a much-loved humour classic. This new definitive edition celebrates the 30th anniversary and ties in with a brand-new book of definitions, Afterliff, from John Lloyd and another old friend of Douglas Adams, Jon Cantor. In life, there are hundreds of familiar experiences, feelings and objects for which no words exist, yet hundreds of strange words are idly loafing around on signposts, pointing at places. The Meaning of Liff connects the two. BERRIWILLOCK (n.) - An unknown workmate who writes 'All the best' on your leaving card. ELY (n.) - The first, tiniest inkling that something, somewhere has gone terribly wrong. GRIMBISTER (n.) - Large body of cars on a motorway all travelling at exactly the speed limit because one of them is a police car. KETTERING (n.) - The marks left on your bottom or thighs after sunbathing on a wickerwork chair. OCKLE (n.) - An electrical switch which appears to be off in both positions. WOKING (ptcpl.vb.) - Standing in the kitchen wondering what you came in here for.",
#     "book_image": "http://books.google.com/books/publisher/content?id=-VMnAAAAQBAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE726BC81m7uBJX022Wx37Wbj9h8Bk-ag4nTga_6MNr10wq59i2N24NeRTtqTtyAyFBnMPXjv5Bgwd4xo_ijRV84gyyFaImCTf4xULrz8w4VwXi_OmF9P67tzxmYYUiDjTJk6YBaD&source=gbs_api"
#   },
#   {
#     "id": 25,
#     "authors": ["Richard Adams"],
#     "genre": "Fiction",
#     "isbn": ["0307950190", "9780307950192"],
#     "language": "en",
#     "publication_date": "2012-12-11",
#     "title": "Tales from Watership Down",
#     "page_count": 267,
#     "ratings": 3.5,
#     "description": "Tales From Watership Down is the enchanting sequel to Richard Adams's bestselling Watership Down, the enduring classic of contemporary literature that introduced millions of readers to a vivid and distinctive world. Here, he returns to the delightful characters we know and love\u2014including Fiver, Hazel, Bigwig, Dandelion, and the legendary rabbit hero El-ahrairah\u2014and presents new heroes as they struggle to survive the cruelties of nature and the shortsighted selfishness of humankind. These whimsical tales include all-new adventures and traditional stories of rabbit mythology, charming us once again with imagination, heart, and wonder. A spellbinding book of courage and survival, Tales From Watership Down is an exciting invitation to come home to a beloved world.",
#     "book_image": "http://books.google.com/books/content?id=XaeFNAu1yvEC&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE72w31Or4hcmeH-TAGknwa59jQ_w370JE3vtw13oALAW9KJogce-lTbk-TNUhAnFJv0aqE4hfDAmx2itRqsimKHHtZ08SNQEty8E_mgwT4gK7n7UDhsbiWBvPFQw0meBaGAZY4-w&source=gbs_api"
#   },
#   {
#     "id": 26,
#     "authors": ["Richard Adams Carey"],
#     "genre": "Nature & Travel & Photography",
#     "isbn": ["9780618056989", "061805698X"],
#     "language": "en",
#     "publication_date": "2000-06-15",
#     "title": "Against the Tide",
#     "page_count": 400,
#     "ratings": 4,
#     "description": "With its spectacular beaches and charming towns, Cape Cod is known around the world as a vacation spot and a summer retreat for the well-to-do. But there is another Cape Cod, a hidden, hardscrabble, year-round world whose hunter-gatherer economy dates back to the Bay Colony. The world of the independent fisherman is one of constant peril, of arcane folkways and expert knowledge, of calculated risk and self-reliance -- and of freedom won daily through backbreaking, solitary work. It is a way of life deep in the American grain. Haunted by the numbers of family fishermen who have recently been forced to abandon the profession, Richard Adams Carey spent a year among a handful of men who stubbornly refuse to do so. Reminiscent of the work of William Warner and Joseph Mitchell, AGAINST THE TIDE is a masterly profile of four New England fishermen in which every page opens onto something more profound: maritime history, maritime ecology, and the poetic celebration of a special American place.",
#     "book_image": "http://books.google.com/books/content?id=YDRDSPFRZocC&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE70r9W14wNRxHx2LWMTaLqFbSyuxtIrdyEvv_AqhvJDlSbnsMBWyhCSAzKOJzAniftC4LnaGVaD1vBehE6uhxNNLex3pwglU_9Z4hA2wgcqUyax-jFTt2f82pa6HMcuE2TLB0Asg&source=gbs_api"
#   },
#   {
#     "id": 27,
#     "authors": ["Richard Adams"],
#     "genre": "Fiction",
#     "isbn": ["1468302027", "9781468302028"],
#     "language": "en",
#     "publication_date": "2001-10-30",
#     "title": "Shardik",
#     "page_count": 604,
#     "ratings": 3,
#     "description": "Richard Adams\u2019s Watership Down was a number one bestseller, a stunning work of the imagination, and an acknowledged modern classic. In Shardik Adams sets a different yet equally compelling tale in a far-off fantasy world. Shardik is a fantasy of tragic character, centered on the long-awaited reincarnation of the gigantic bear Shardik and his appearance among the half-barbaric Ortelgan people. Mighty, ferocious, and unpredictable, Shardik changes the life of every person in the story. His advent commences a momentous chain of events. Kelderek the hunter, who loves and trusts the great bear, is swept up by destiny to become first devotee and then prophet, then victorious soldier, then ruler of an empire and priest-king of Lord Shardik\u2014Messenger of God\u2014only to discover ever-deeper layers of meaning implicit in his passionate belief in the bear\u2019s divinity.",
#     "book_image": "http://books.google.com/books/publisher/content?id=exKEDwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70SCmaAj7dm22B9R869nSC9FPlpYwB3eT_Fmbr99_IbtUIBqBXBs8fdekpFkHi4EkNherobl8hyzgEcOuVGH_ePaGVFPLLDawxBJGCm2YFOt7WOlEEpkrt05YjwtJF1djaGaFUU&source=gbs_api"
#   },
#   {
#     "id": 28,
#     "authors": ["Richard Adams"],
#     "genre": "Nature & Travel & Photography",
#     "isbn": ["0140057161", "9780140057164"],
#     "language": "en",
#     "publication_date": "1986",
#     "title": "A Nature Diary",
#     "page_count": 158,
#     "ratings": 3.5,
#     "description": "N/A",
#     "book_image": "http://books.google.com/books/content?id=ePbF2tLpR6cC&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE70o0kA7lH9QMTG2fqeNyMbTJZ-pxiDXAwWUx24ouYqiOqQnBO_Q299_05tByDiIn0d7ydU0xsCCU_xLZImQEhVjOpCfgS87owYVbAzAyZ_uh25dv9Odr8NwcQ-FyiKOcJ6E0eht&source=gbs_api"
#   },
#   {
#     "id": 29,
#     "authors": ["John Agard"],
#     "genre": "Poetry",
#     "isbn": ["1852247339", "9781852247331"],
#     "language": "en",
#     "publication_date": "2006",
#     "title": "We Brits",
#     "page_count": 69,
#     "ratings": 0,
#     "description": "John Agard has been subverting British poetry for the past 30 years with his mischievous, satirical fables which overturn all our expectations. In \"We Brits\", the Guyanese-born word magician gives an outsider's inside view of British life in poems which both challenge and cherish our peculiar culture and hallowed institutions. Some explore hidden connections in British history, while others are wildly inventive forays into comic territory: Shakespeare addresses the tabloids, Jesus, Buddha and Mohammed arrive in Britain at Gatwick, Heathrow and Dover, and all the foreign words flee the English dictionary.",
#     "book_image": "http://books.google.com/books/content?id=r4RlAAAAMAAJ&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE7294ed647Hw1bsD1_kY2xzwdA_FslY4eMMZeY2eD0JAzJkQcQaKq5pvoBmbcMSLWmKGf2swctCa0Fb9s7RyO8QPPxVYHqvR1wc1bj7eqYdjMGYy1z8eBTzrkvMO1YOpOIZVzAWv&source=gbs_api"
#   },
#   {
#     "id": 30,
#     "authors": ["John Agard"],
#     "genre": "Poetry",
#     "isbn": ["1852244062", "9781852244064"],
#     "language": "en",
#     "publication_date": "1997",
#     "title": "From the Devil's Pulpit",
#     "page_count": 127,
#     "ratings": 0,
#     "description": "In these new poems, John Agard writes From the Devil's Pulpit, giving a Devil's eye view of the world, sweeping from Genesis across time. A sometimes poignant, bawdy, witty and sophisticated shape-shifter, even sex-switcher, Agard's Devil engages with the world of myth, metaphysics, theology, politics and the arts - not to mention Wimbledon Centre Court and Lords cricket ground.",
#     "book_image": "http://books.google.com/books/content?id=sC5aAAAAMAAJ&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE70s1X9lBKQ_GaQRCaPs794hxWJKtZPJDBb6JcDEtTACFMQN35Dqtrn_edRjA_0C1CcoEeAzKHCshXU1I9yjTf9i2lB5wABHdV336gJqyutO9jq6FKTcDeXSc1vXGPnc8-hxT5qx&source=gbs_api"
#   },
#   {
#     "id": 31,
#     "authors": ["John Agard"],
#     "genre": "Fiction",
#     "isbn": ["0571324150", "9780571324156"],
#     "language": "en",
#     "publication_date": "2017-09-05",
#     "title": "Come All You Little Persons",
#     "page_count": 32,
#     "ratings": 0,
#     "description": "A tender, moving and joyous story from one of our nation's premier poets, John AgardFrom above earth, from above sky,from below earth, from under water,come all you little personscome exactly as you are.Come little bird person, come little bee person, come little tree person - little persons from all over the world join together to celebrate the dance of life and love in this stunning poem from John Agard. Stunningly illustrated by Jessica Courtney-Tickle, this is a book that both little persons and big persons will treasure and pore over for a lifetime, and is a true poem of our time.'A lyrical collaboration . . . a joyful, tender dance, celebrating people, places and the glory of existence.' Guardian'This book is a celebration of childhood and the natural world in all its rich diversity.' BookTrust",
#     "book_image": "http://books.google.com/books/publisher/content?id=jeklDwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE72xod6GlhVsQufpF00Pd5IqhYYKrRgi2GAP5npCrCOEVGZzFhjAxHqKhliHgtrkXbC3iaFxiICwRezXg79QILy97WaSAD0w7vokzNWu-qydgNMg1gWs_fjlf_ohqMlQA8i6rU0D&source=gbs_api"
#   },
#   {
#     "id": 32,
#     "authors": ["John Agard"],
#     "genre": "Fiction",
#     "isbn": ["1913747611", "9781913747619"],
#     "language": "en",
#     "publication_date": "2021-04-06",
#     "title": "Coyote's Soundbite",
#     "page_count": 40,
#     "ratings": 0,
#     "description": "A rip-roaring poem about protecting our planet, told through the eyes of Coyote the trickster. Excitement spreads like wildfire through the jungle. Earth-goddesses are planning a conference! From Australia to Antarctica, Amazon to Africa, goddesses will debate the burning environmental issues of our times . . . and bushy-tailed, smooth-talking Coyote wants in on the action. Can this infamous trickster come up with a plan to infiltrate the conference and leave a lasting legacy for our planet?  \u201cQuirky and inventive. The writing is beautiful. The illustrations truly shine and will be enjoyed by all. An aesthetically pleasing and stimulating work of art\u201d\u2014ALA Booklist \u201cAgard\u2019s words and Grobler\u2019s illustrations harmoniously deliver an engaging and timely tale. Humans have indeed become blind to the fragile beauty of the Earth. This story, packed full of playful characters that fly and dance around the page as creation myths are told, asks young readers to take a different view and to love the world around them\u201d\u2014Andy Robert Davies, children's illustrator \u201cThe more I read this book the more I wanted to read it again because every time I looked at it again I noticed a new drawing or found something to use in my own writing. This is an important story to tell because lots of bad things are happening to our world currently and it reminds us of what we need to do to help our planet\u201d\u2014Lottie Kid Reviewer, Books Up North",
#     "book_image": "http://books.google.com/books/publisher/content?id=sJkiEAAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE72sBjXWlVF7t97RdHXTewp0v8ijzrUnofH_3cK-rkOkqHQLWh_3ECL70rpAlku1j8BMujbmSSZiOzP1iu_EaxBW8sXPwBSAbvSUbIFglVHNmzFkSPYrOE__dDGUNuadZeM3Qhat&source=gbs_api"
#   },
#   {
#     "id": 33,
#     "authors": ["James Agee"],
#     "genre": "Fiction",
#     "isbn": ["014310571X", "9780143105718"],
#     "language": "en",
#     "publication_date": "2009-09-29",
#     "title": "A Death in the Family",
#     "page_count": 320,
#     "ratings": 4,
#     "description": "The classic American novel\u2014winner of the 1958 Pulitzer Prize\u2014now re-published for the 100th anniversary of James Agee\u2019s birthOne of\u00a0Time\u2019s All-Time 100 Best NovelsA Penguin Classic Published in 1957, two years after its author's death at the age of forty-five,\u00a0A Death in the Family\u00a0remains a near-perfect work of art, an autobiographical novel that contains one of the most evocative depictions of loss and grief ever written. As Jay Follet hurries back to his home in Knoxville, Tennessee, he is killed in a car accident\u2014a tragedy that destroys not only a life, but also the domestic happiness and contentment of a young family. A novel of great courage, lyric force, and powerful emotion,\u00a0A Death in the Family\u00a0is a masterpiece of American literature.",
#     "book_image": "http://books.google.com/books/publisher/content?id=46-KDQAAQBAJ&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE73-O47wJqV57GUB6HMzgtxgYB2xgjSXMPGaNdpyYXckJAh-V5XJTVJJ6CUFee8Rq4SL3Hg_FRyhtLX2yM_mKbTx6EgzUEE0df7JLqm1TpCi6OG9emTmDS0Pt9E_osUH7KXwoMtg&source=gbs_api"
#   },
#   {
#     "id": 34,
#     "authors": ["James Agee", "Walker Evans"],
#     "genre": "History",
#     "isbn": ["9780618127498", "0618127496"],
#     "language": "en",
#     "publication_date": "2001",
#     "title": "Let Us Now Praise Famous Men",
#     "page_count": 416,
#     "ratings": 4,
#     "description": "The American classic, in words and photographs, of three tenant families in the deep South. Published nearly sixty years ago, Let Us Now Praise Famous Men stands as an undisputed American masterpiece, taking its place alongside works by Henry David Thoreau, Herman Melville, and Walt Whitman. In a stunning blend of prose and images, this classic offers at once an unforgettable portrait of three tenant families in the Deep South and a larger meditation on human dignity and the American soul. In the summer of 1936, James Agee and Walker Evans set out on assignment for Fortune magazine to explore the daily lives of sharecroppers in the South. There they lived with three different families for a month; the result of their stay was an extraordinary collaboration, an unsparing record of place, of the people who shaped the land, and of the rhythm of their lives. Upon its first book publication in 1941, Let Us Now Praise Famous Men was called intensely moving, unrelentingly honest. It described a mode of life -- and rural poverty -- that was unthinkably remote and tragic to most Americans, and yet for Agee and Evans, only extreme realism could serve to make the world fully aware of such circumstances. Today it stands as a poetic tract for its time, a haunting search for the human and religious meaning in the lives of true Southern heroes: in their waking, sleeping, eating; their work; their houses and children; and their endurance. With an elegant design and a sixty-four-page photographic prologue of Evans's stunning images, reproduced from archival negatives, the new edition introduces the legendary author and photographer to a new generation. Both an invaluable part of the American heritage and a graceful tribute to the vibrant souls whose stories live in these pages, this book has profoundly changed our culture and our consciousness -- and will continue to inspire for generations to come.",
#     "book_image": "http://books.google.com/books/content?id=qrmm6yNCZysC&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE73Y3sDheFEvILtnbBj3tmW_e0B5EnF_3OgTcDwUXeTpxJNF6zn9ZtSVA_VmAU7P3RG_p-tqU-UZ_pZAJcBJ3g-fCLiHCWIbzDLLE0XCCXSglzYDOJ3mvE9RhGBuP3htrSRrh5Mx&source=gbs_api"
#   },
#   {
#     "id": 35,
#     "authors": ["James Agee"],
#     "genre": "Fiction",
#     "isbn": ["1931082812", "9781931082815"],
#     "language": "en",
#     "publication_date": "2005-09-22",
#     "title": "James Agee: Let Us Now Praise Famous Men / A Death in the Family / shorter fiction (LOA #159)",
#     "page_count": 818,
#     "ratings": 4.5,
#     "description": "A passionate literary innovator, eloquent in language and uncompromising in his social observation and his pursuit of emotional truth, James Agee (1909\u20131955) excelled as novelist, critic, journalist, and screenwriter. In his brief, often turbulent life, he left enduring evidence of his unwavering intensity, observant eye, and sometimes savage wit. This Library of America volume collects his fiction along with his extraordinary experiment in what might be called prophetic journalism,\u00a0Let Us Now Praise Famous Men\u00a0(1941), a collaboration with photographer Walker Evans that began as an assignment from\u00a0Fortune\u00a0magazine to report on the lives of Alabama sharecroppers, and that expanded into a vast and unique mix of reporting, poetic meditation, and anguished self-revelation that Agee described as \u201can effort in human actuality.\u201d A sixty-four-page photo insert reproduces Evans\u2019s now-iconic photographs from the expanded 1960 edition.A Death in the Family, the Pulitzer Prize\u2013winning novel that he worked on for over a decade and that was published posthumously in 1957, recreates in stunningly evocative prose Agee\u2019s childhood in Knoxville, Tennessee, and the upheaval his family experienced after his father\u2019s death in a car accident when Agee was six years old. A whole world, with its sensory vividness and social constraints, comes to life in this child\u2019s-eye view of a few catastrophic days. It is presented here for the first time in a text with corrections based on Agee\u2019s manuscripts at the Harry Ransom Humanities Research Center.This volume also includes\u00a0The Morning Watch\u00a0(1951), an autobiographical novella that reflects Agee\u2019s deep involvement with religious questions, and three short stories: \u201cDeath in the Desert,\u201d \u201cThey That Sow in Sorrow Shall Not Reap,\u201d and the remarkable allegory \u201cA Mother\u2019s Tale.\u201dLIBRARY OF AMERICA\u00a0is an independent nonprofit cultural organization founded in 1979 to preserve our nation\u2019s literary heritage by publishing, and keeping permanently in print, America\u2019s best and most significant writing. The Library of America series includes more than 300 volumes to date, authoritative editions that average 1,000 pages in length, feature cloth covers, sewn bindings, and ribbon markers, and are printed on premium acid-free paper that will last for centuries.",
#     "book_image": "http://books.google.com/books/publisher/content?id=L6lNEAAAQBAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE70KCTMeWd5FzccG7ALCxh8s0AEAWFTPiTQEKoYD2zWUkan4TpdiPUJ_3RgEYyuIjKEn6CpSZhJQX3z4L7O6yV_dWyBc7JOUmN0MpoACsMrYdt57BFkcEdAYAzqi9REvdIjqJ_PU&source=gbs_api"
#   },
#   {
#     "id": 36,
#     "authors": ["James Agee"],
#     "genre": "History",
#     "isbn": ["1612192122", "9781612192123"],
#     "language": "en",
#     "publication_date": "2013",
#     "title": "Cotton Tenants",
#     "page_count": 224,
#     "ratings": 0,
#     "description": "A re-discovered masterpiece of reporting by a literary icon and a celebrated photographerIn 1941, James Agee and Walker Evans published Let Us Now Praise Famous Men, a four-hundred-page prose symphony about three tenant farming families in Hale County, Alabama at the height of the Great Depression. The book shattered journalistic and literary conventions. Critic Lionel Trilling called it the \u201cmost realistic and most important moral effort of our American generation.\u201dThe origins of Agee and Evan's famous collaboration date back to an assignment forFortune magazine, which sent them to Alabama in the summer of 1936 to report a story that was never published. Some have assumed thatFortune's editors shelved the story because of the unconventional style that markedLet Us Now Praise Famous Men, and for years the original report was lost.But fifty years after Agee's death, a trove of his manuscripts turned out to include a typescript labeled \u201cCotton Tenants.\u201d Once examined, the pages made it clear that Agee had in fact written a masterly, 30,000-word report forFortune.Published here for the first time, and accompanied by thirty of Walker Evans's historic photos,Cotton Tenants is an eloquent report of three families struggling through desperate times. Indeed, Agee's dispatch remains relevant as one of the most honest explorations of poverty in America ever attempted and as a foundational document of long-form reporting. As the novelist Adam Haslett writes in an introduction, it is \u201ca poet's brief for the prosecution of economic and social injustice.\u201dCo-Published with The Baffler magazine",
#     "book_image": "http://books.google.com/books/publisher/content?id=x8NvDwAAQBAJ&printsec=frontcover&img=1&zoom=4&edge=curl&imgtk=AFLRE722Roip5nBW1QS-cd6xKlMxNGcT_UPxS18zEVt4Y5W2g4HQiYEbiJ8QmMF1eCBk0loG1H7JpeswNdlPBVODopOHawKCkA8r6ChRRzpwHMGFSVKjgO5qQa2_umSzxIuY_f-YLqyw&source=gbs_api"
#   },
#   {
#     "id": 37,
#     "authors": ["Ama Ata Aidoo"],
#     "genre": "Fiction",
#     "isbn": ["1558619143", "9781558619142"],
#     "language": "en",
#     "publication_date": "2015-04-25",
#     "title": "Changes",
#     "page_count": 208,
#     "ratings": 3,
#     "description": "A Commonwealth Prize\u2013winning novel of \u201cintense power\u00a0.\u00a0.\u00a0. examining the role of women in modern African society\u201d by the acclaimed Ghanaian author\u00a0(Publishers Weekly). \u00a0 Living in Ghana\u2019s capital city of Accra with a postgraduate degree and a career in data analysis, Esi Sekyi is a thoroughly modern African woman. Perhaps that is why she decides to divorce her husband after enduring yet another morning\u2019s marital rape. Though her friends and family are baffled by her decision (after all, he doesn\u2019t beat her!), Esi holds fast. When she falls in love with a married man\u2014wealthy, and able to arrange a polygamous marriage\u2014the modern woman finds herself trapped in a new set of problems. \u00a0 Witty and compelling, Aidoo\u2019s novel, according to Manthia Diawara, \u201cinaugurates a new realist style in African literature.\u201d In an afterword to this edition, Tuzyline Jita Allan \u201cplaces Aidoo\u2019s work in a historical context and helps introduce this remarkable writer [who] sheds light on women\u2019s problems around the globe\u201d (Publishers Weekly).",
#     "book_image": "http://books.google.com/books/publisher/content?id=izO3BwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE736JEY7iaLBjWUFKXtOPldbY9tO3nDng-pHYp2bz3ej7d3Pf1pihUdKGJoMCoJYMw8-VANFaZ2EdfzyxBMn5ZitXj8EGNUCTpcTb2J-4I2GiZEaGGRx7uOZtqNpXeIRdQasALaZ&source=gbs_api"
#   },
#   {
#     "id": 38,
#     "authors": ["Ama Ata Aidoo"],
#     "genre": "Fiction",
#     "isbn": ["155861916X", "9781558619166"],
#     "language": "en",
#     "publication_date": "2015-04-25",
#     "title": "No Sweetness Here",
#     "page_count": 172,
#     "ratings": 5,
#     "description": "From the author of Changes: these stories \u201cof post-independence Ghana in the late 1960s are written beautifully and wisely and with great subtlety\u201d (Chimamanda Ngozi Adichi). \u00a0 In this short story collection, the award-winning poet and author of Changes and Our Sister Killjoy explores postcolonial life in Ghana with her characteristic honesty, humor, and insight. A house servant wonders what independence means in a country where indoor plumbing is still reserved for bosses. A brother tracks down his runaway sister only to find she has become a prostitute. In the title story, a bitter divorce turns tragic when the couple\u2019s only child dies of a snake bite. \u00a0 In these and other stories, tradition wrestles with new urban influences as Africans try to sort out their identity in a changing culture, and \u201ceven at her gravest, Miss Aidoo writes with a sunny charm\u201d (The New York Times).",
#     "book_image": "http://books.google.com/books/publisher/content?id=1TO3BwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE73dMVB6WShsur5JWZhrCLPRaLk95UgGiJVCr2UC1Br7vt4nuWIvl0j9tF_A736C78Q5aFJSjCdko5rPtND38tYxcNVR54iKbKHIwdXYP-V8FDGGkf7jbNbQfckjKFOSvF03UMgO&source=gbs_api"
#   },
#   {
#     "id": 39,
#     "authors": ["Conrad Aiken"],
#     "genre": "Fiction",
#     "isbn": ["1504011406", "9781504011402"],
#     "language": "en",
#     "publication_date": "2015-06-02",
#     "title": "Great Circle",
#     "page_count": 129,
#     "ratings": 0,
#     "description": "A profound examination of the mysteries of memory and perception from one of the twentieth century\u2019s most admired literary artists The train races from New York to Boston. For Andrew Cather, it is much too fast. He will return home three days early, and he is both terrified and intrigued by what he may find there. He pictures himself unlocking the door to his quiet Cambridge house, padding silently through its darkened halls, and finally discovering the thing he both fears and yearns to see: his wife in the arms of another man. Cather knows that what he finds in Cambridge may destroy his life, yet finally set him free. \u00a0 A masterful portrait of an average man at the edge of a shocking precipice,\u00a0Great Circle\u00a0is a triumph of psychological realism. One of Sigmund Freud\u2019s favorite novels, it is a probing exploration of the secrets of consciousness.",
#     "book_image": "http://books.google.com/books/publisher/content?id=FeLuBwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70h7nehlA44jFULYovRBbEZOEqaknfL6NnvHwYgdhJyy0DqRP7gHrchchg4A9qf0fFvy5NuDeNNUE6HaTFEXCTeE48nvgAxP4KwqHqzCiX-RBDo031o-ZBz76qJ1-_TrsFRUQzU&source=gbs_api"
#   },
#   {
#     "id": 40,
#     "authors": ["Conrad Aiken"],
#     "genre": "Poetry",
#     "isbn": ["EAN:4057664616357"],
#     "language": "en",
#     "publication_date": "2019-11-29",
#     "title": "The House of Dust: A Symphony",
#     "page_count": 148,
#     "ratings": 0,
#     "description": "\"The House of Dust: A Symphony\" by Conrad Aiken. Published by Good Press. Good Press publishes a wide range of titles that encompasses every genre. From well-known classics & literary fiction and non-fiction to forgotten\u2212or yet undiscovered gems\u2212of world literature, we issue the books that need to be read. Each Good Press edition has been meticulously edited and formatted to boost readability for all e-readers and devices. Our goal is to produce eBooks that are user-friendly and accessible to everyone in a high-quality digital format.",
#     "book_image": "http://books.google.com/books/publisher/content?id=vxrDDwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE720V5pYfHOOWOKzov5zeO_hG2GK3dSvd9nsYUd0bXIF_u0PktCJJgNU1M-ZhZba-K4nYGkty5aVPsHiKrB01IYkMg66CII5wJrAu71eQmfqSvG7fBxw0ECCHqFMYfWawc9halar&source=gbs_api"
#   },
#   {
#     "id": 41,
#     "authors": ["Conrad Aiken"],
#     "genre": "Fiction",
#     "isbn": ["1480420050", "9781480420052"],
#     "language": "en",
#     "publication_date": "2015-06-02",
#     "title": "The Collected Short Stories of Conrad Aiken",
#     "page_count": 566,
#     "ratings": 3,
#     "description": "This indispensable volume, which includes the classic stories \u201cSilent Snow, Secret Snow\u201d and \u201cMr. Arcularis,\u201d is a testament to the dazzling artistry of one of the twentieth century\u2019s most influential writers A young woman passes through the countryside to visit her dying grandmother for a final time. A cabbie, exhausted from a long day\u2019s work, fights to get an intoxicated woman out of his taxi. A man on his way to a bachelor party tries to come to grips with the brutishness that lies within every gentleman\u2014and finds that Bacardi cocktails do nothing to help. \u00a0 A master craftsman whose poetry and prose offer profound insight into the riddle of consciousness, Conrad Aiken thrills, disturbs, and inspires in all forty-one of these astute and eloquent tales.",
#     "book_image": "http://books.google.com/books/publisher/content?id=oN_uBwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE72tqyFll6B3D9FYwDAY2-GVo8Ds2an7C8olghNabcShNqBYWSVFbn__ibeF9igPWRi9m8W3DEUFwED49isavtqfWWWhUP9FNFSlivXvFfF0L8wI5UFq-J4ELCSnPqrGg37pbV0O&source=gbs_api"
#   },
#   {
#     "id": 42,
#     "authors": ["Conrad Aiken"],
#     "genre": "Poetry",
#     "isbn": ["0828314438", "9780828314435"],
#     "language": "en",
#     "publication_date": "1953-12",
#     "title": "Jig of Forslin",
#     "page_count": 79,
#     "ratings": 0,
#     "description": "Originally published in 1916. This volume from the Cornell University Library's print collections was scanned on an APT BookScan and converted to JPG 2000 format by Kirtas Technologies. All titles scanned cover to cover and pages may include marks notations and other marginalia present in the original volume.",
#     "book_image": "http://books.google.com/books/content?id=8wDczgEACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE71YUqyDOTcgflJ1EKyKWnTB1OG7mEUv2DYK4Uqm3asCI833oj0mCpXbkInHGFt7HN7RP8m-9ZTrwaWh1B7bHIivermIN6CA6zDizu-H_-mBFpCjncaoENkc1GPRQVqT_1xl1Z3u&source=gbs_api"
#   },
#   {
#     "id": 43,
#     "authors": ["Anna Akhmatova"],
#     "genre": "Poetry",
#     "isbn": ["0804040842", "9780804040846"],
#     "language": "en",
#     "publication_date": "2017-08-14",
#     "title": "You Will Hear Thunder",
#     "page_count": 160,
#     "ratings": 0,
#     "description": "Anna Akhmatova lived through pre-revolution Russia, Bolshevism, and Stalinism. Throughout it all, she maintained an elegant, muscular style that could grab a reader by the throat at a moment\u2019s notice. Defined by tragedy and beauty in equal measure, her poems take on romantic frustration and the pull of the sensory, and find power in the mundane. Above all, she believed that a Russian poet could only produce poetry in Russia.You Will Hear Thunder spans Akhmatova\u2019s very early career into the early 1960s. These poems were written through her bohemian prerevolution days, her many marriages, the terror and privation of life under Stalin, and her later years, during which she saw her work once again recognized by the Soviet state. Intricately observed and unwavering in their emotional immediacy, these strikingly modern poems represent one of the twentieth century\u2019s most powerful voices.",
#     "book_image": "http://books.google.com/books/publisher/content?id=VQkvDwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE71S7ZEUSyZyeuhsy2c0qLDFTo8TKxUJvlunSjx5CL7n1dyqJv3qWt-BCsD_ADgRIUhtt33AnLULHFmQPXfVv8WOn50gikTQgrzbWSgY4GJN3Dqzf8CKV8G0vpg9zmwdAfEKOr64&source=gbs_api"
#   },
#   {
#     "id": 44,
#     "authors": ["Anna Akhmatova"],
#     "genre": "Poetry",
#     "isbn": ["080404094X", "9780804040945"],
#     "language": "en",
#     "publication_date": "2018-09-19",
#     "title": "Way of All the Earth",
#     "page_count": 98,
#     "ratings": 0,
#     "description": "Anna Akhmatova is considered one of Russia\u2019s greatest poets. Her life encompassed the turmoil of the Russian Revolution and the paranoia and persecution of the Stalinist era: her works embody the complexities of the age. At the same time, she was able to merge these complexities into a single, poetic voice to speak to the Russian people with whom she so closely and proudly identified.Way of All the Earth contains short poems written between 1909 and 1964, selected from Evening, Rosary, White Flock, Plantain, Anno Domini, Reed, and The Seventh Book. Intricately observed and unwavering in their emotional immediacy, these strikingly modern poems represent one of the twentieth century\u2019s most powerful voices.",
#     "book_image": "http://books.google.com/books/publisher/content?id=JUdvDwAAQBAJ&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE72jGQDPwiAIya4HptO0KgX3rE2od_tnINtr3D3bdszd-oKJ2Ut6bEvKIVrxJCR_pYX4S6kT-q2xPWSlVFHJNQaxGu6BhlvfthmZ0RyvdmztO3uQO2UloGddKcNFuua9x111MeDz&source=gbs_api"
#   },
#   {
#     "id": 45,
#     "authors": ["Ry\u016bnosuke Akutagawa"],
#     "genre": "Fiction",
#     "isbn": ["1568860617", "9781568860619"],
#     "language": "en",
#     "publication_date": "1999",
#     "title": "The Essential Akutagawa",
#     "page_count": 207,
#     "ratings":0,
#     "description": "Akutagawa's voice is one of the most remarkable in modern Japanese fiction: an acutely intelligent mind, a humiliated soul, engaging as readily with Baudelaire as with Confucius. These narratives and vignettes -- some having received little attention until now -- delicately dovetail ancient myth with modern reflection. Akutagawa combines Eastern sentiment with Western thought to astonishing effect, offering a uniquely moving insight into mental and social fragmentation.",
#     "book_image": "http://books.google.com/books/content?id=18xkAAAAMAAJ&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE71wAyW85sNlP1cdCRyIXrTMPVY8fLmER56X32GZMkqlBD4gPnpQfVil7lLb8LHpfDC6fFsDMWXQTH2ghoJKwxV_279Wtb0XWgIhfLlEEp-zZLlIb0HraxbAyoQFcuWtD92bv2zM&source=gbs_api"
#   },
#   {
#     "id": 46,
#     "authors": ["Ry\u016bnosuke Akutagawa", "\u82a5\u5ddd\u9f8d\u4e4b\u4ecb"],
#     "genre": "Fiction",
#     "isbn": ["0871401738", "9780871401731"],
#     "language": "en",
#     "publication_date": "1952",
#     "title": "Rashomon",
#     "page_count": 119,
#     "ratings": 4,
#     "description": "Writing at the beginning of the twentieth century, Ryunosuke Akutagawa created disturbing stories out of Japan's cultural upheaval. Whether his fictions are set centuries past or close to the present, Akutagawa was a modernist, writing in polished, superbly nuanced prose subtly exposing human needs and flaws. \"In a Grove,\" which was the basis for Kurosawa's classic film Rashomon, tells the chilling story of the killing of a samurai through the testimony of witnesses, including the spirit of the murdered man. The fable-like \"Yam Gruel\" is an account of desire and humiliation, but one in which the reader's sympathy is thoroughly unsettled. And in \"The Martyr,\" a beloved orphan raised by Jesuit priests is exiled when he refuses to admit that he made a local girl pregnant. He regains their love and respect only at the price of his life. All six tales in the collection show Akutagawa as a master storyteller and an exciting voice of modern Japanese literature.",
#     "book_image": "http://books.google.com/books/content?id=IkSwwF542DAC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70BUle8Yjsq_OXilriYxHHpbnADHvHorCYJR_i-lovbjxMSIBpGEw5OCAVU4XNyWGg_X_F01Diqy_mhkZf3pow7TM3cA0DKS1xMv5RdByBaqJdlBbVIHFu3O_cV6Oa2c1-6u412&source=gbs_api"
#   },
#   {
#     "id": 47,
#     "authors": ["Henri Alain-Fournier"],
#     "genre": "Fiction",
#     "isbn": ["0141921706", "9780141921709"],
#     "language": "en",
#     "publication_date": "2007-05-03",
#     "title": "The Lost Estate (Le Grand Meaulnes)",
#     "page_count": 256,
#     "ratings": 0,
#     "description": "'I read it for the first time when I was seventeen and loved every page. I find its depiction of a golden time and place just as poignant now as I did then' Nick HornbyThe Lost Estate is Robin Buss's translation of Henri Alain-Fournier's poignant study of lost love, Le Grand Meaulnes.When Meaulnes first arrives at the local school in Sologne, everyone is captivated by his good looks, daring and charisma. But when Meaulnes disappears for several days, and returns with tales of a strange party at a mysterious house - and his love for the beautiful girl hidden within it, Yvonne de Galais - his life has been changed forever. In his restless search for his Lost Estate and the happiness he found there, Meaulnes, observed by his loyal friend Francois, may risk losing everything he ever had. Poised between youthful admiration and adult resignation, Alain-Fournier's compelling narrator carries the reader through this evocative and unbearably poignant portrayal of desperate friendship and vanished adolescence. Robin Buss's translation of Le Grand Meaulnes sensitively and accurately renders Alain-Fournier's poetically charged, expressive and deceptively simple style. In his introduction, New Yorker writer Adam Gopnik discusses the life of Alain-Fournier, who was killed in the First World War after writing this, his only novel.If you liked Le Grand Meaulnes, you might enjoy Gustave Flaubert's Sentimental Education, also available in Penguin Classics.",
#     "book_image": "http://books.google.com/books/content?id=jekvPAShQogC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE707qX1hb_ZGxWOs5GtUYyghFfm66gT9Q5XcanQZOuiXQ5KYt6_oCZtBct42QIbjlDeB3E0bBWeX4Ysf_wZFOsoU8n4hIETaxbk1rS9Tyj1oAbPl6KaAP3nXBiqBgrwiOLBRRbdr&source=gbs_api"
#   },
#   {
#     "id": 48,
#     "authors": ["Alain-Fournier"],
#     "genre": "Fiction",
#     "isbn": ["0140182829", "9780140182828"],
#     "language": "en",
#     "publication_date": "1990-03",
#     "title": "Le Grand Meaulnes",
#     "page_count": 206,
#     "ratings": 3.5,
#     "description": "The classic French novel written by a soldier, who would later die during World War I, tells the story of Auguste Meaulnes and the \"domain mysterieux.\"",
#     "book_image": "http://books.google.com/books/content?id=fuL-_BhZFyEC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE71gmGBb74JyMkmtgIJ4-47cW-5nSEoCQjsxzoeLO30gchIHaAmu8KuaLQMUHjyEmn_YSpek5fm6KC55p2KhkRI0wXV6gPRPodWbElIbRNAl3FSRy2BwwEFiVTf5LsES_cKGw42x&source=gbs_api"
#   },
#   {
#     "id": 49,
#     "authors": ["Edward Albee"],
#     "genre": "Drama",
#     "isbn": ["0822214210", "9780822214212"],
#     "language": "en",
#     "publication_date": "1995",
#     "title": "Edward Albee's Fragments",
#     "page_count": 57,
#     "ratings": 0,
#     "description": "THE STORY: Several people sit together reading proverbs aloud to each other. From these proverbs are prompted stories of each one's past, or musings surrounding lifelong mysteries. Each tries to tell about some incident which they hope will illumin",
#     "book_image": "http://books.google.com/books/content?id=ft0M2qa0hr0C&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE70iilqLToL-FTdvTIINK-_Q8Vsrne4Rf5tDKK-z7JKgsr9KQeyPEiktfKEZ6_AisDFanQgSm6T1fN0DYHnJNtGlYkWma70TQfhvkTZYq3kkfW3UBvFtGfLHpn4NNrw-0XTtLqDa&source=gbs_api"
#   },
#   {
#     "id": 50,
#     "authors": ["Edward Albee"],
#     "genre": "Drama",
#     "isbn": ["0822202425", "9780822202424"],
#     "language": "en",
#     "publication_date": "1978",
#     "title": "Counting the Ways and Listening",
#     "page_count": 71,
#     "ratings": 0,
#     "description": "Listening. Constructed with the precision of a musical composition, and described by Clive Barnes as \"a chamber opera and a symbolic poem about communication,\" the play juxtaposes three characters - \"The Man,\" \"The Woman,\" and \"The Girl\" - and sifts through the tangled relationship they have evidently shared. The Man is amiable but distant; The Woman acerbic and bitter; The Girl is perhaps mad - a catatonic who has destroyed her own child. Elliptical in form and redolent with evocative overtones, the play weaves together its strands of conversation and soliloquy into a meaningful pattern of events - underscoring the inescapable fact that while we may listen we do not always hear, and our lives, for better or worse, are shaped accordingly. (1 man, 2 women.).",
#     "book_image": "http://books.google.com/books/content?id=ojgc_2OVZksC&printsec=frontcover&img=1&zoom=6&edge=curl&imgtk=AFLRE71Swi70KHo0hSZlo2Pf4TB-P6tKDyjHHnwIoEO26NrELF-0l1mV-yq2Ehv4NE30mdVkUhG7HAHjd7DyEV2kGMfi1UfhRiS3y8TmKg4jPgE-Wz2Jh865occhXrtTVVtasSbyvbPE&source=gbs_api"
#   },
#   {
#     "id": 51,
#     "authors": ["John Beech", "Edward Albee"],
#     "genre": "Art",
#     "isbn": ["0935875239", "9780935875232"],
#     "language": "en",
#     "publication_date": "2008",
#     "title": "Obscure/reveal",
#     "page_count": 40,
#     "ratings": 0,
#     "description": "New York-based British artist John Beech and New York playwright Edward Albee have known each other since 1991, when Albee acquired the first of several of Beech's works for his collection. In the summer of 2006, Beech set out for Montauk to meet with Albee about collaborating on a book. As Albee went through the images, Beech was struck by the poignant and poetic remarks Albee spoke in response, sometimes with just a single word, at other times with a short insightful phrase. These primary responses were the genesis of the facsimile-reproduced handwritten texts that appear in response to each of the 40 images reproduced in this stunning limited edition of 750 copies.",
#     "book_image": "http://books.google.com/books/content?id=hSxkPgAACAAJ&printsec=frontcover&img=1&zoom=1&imgtk=AFLRE732P4wD9nAInHjjbrMJCD3IQ39epN-0ZtJt7IBwNFvU_cegm043LAleV96VV1TpFVR-5KI-wNiBbG6-_8kMeFYDnSvUMnrb-7Ua1XMWRk6ASJBtS3MfYIpDAlY1pa3D_M4LmGwO&source=gbs_api"
#   },
#   {
#     "id": 52,
#     "authors": ["Edward Albee", "Anderson Gallery", "Harry Rand"],
#     "genre": "Art",
#     "isbn": ["0935519238", "9780935519235"],
#     "language": "en",
#     "publication_date": "2000",
#     "title": "From Idea to Matter",
#     "page_count": 62,
#     "ratings": 0,
#     "description": "From Idea to Matter brings Edward Albee, America's foremost playwright, to the visual arts stage. The book documents a unique collaboration between Albee and the Anderson Gallery to bring together the work of nine contemporary sculptors -- John Beech, John Duff, David Fulton, Barry Goldberg, David McDonald, Richard Nonas, Mia Westerlund Roosen, Jonathan Thomas, and Paul Whiting. Albee has selected the artists, chosen the work, and designed an exhibition installation.From Idea to Matter contains full color images of the artists' work, an introduction by Albee, an insightful conversation between Albee and Harry Rand, art historian from the Smithsonian Institution, and an in-depth discussion of the artists' work by Rand.Edward Albee has been visiting museums, art galleries, art studios, and collecting art for the past 40 years, developing a strong personal visual opinion and aesthetic. His approach to the curatorial process can be compared to the artists' dedication to their sculpture, and is equally sure and confident.",
#     "book_image": "http://books.google.com/books/content?id=oxZQAAAAMAAJ&printsec=frontcover&img=1&zoom=6&imgtk=AFLRE712tyQaVPEk90ru89yaMDToAUzRUIlXAdXCOUkXXUHMk7VhZSNn_eHYqFzn7KQg5CtchMFplMsZ8iiFGgJKyWSQPnJZBgsN1Kt4KgnVYtEci0yo8kKzh2723Y9zDTmZEcsjUOGJ&source=gbs_api"
#   }
# ]

# # data for 3 authors
# authors = [{
#     "id": 1,
#     "name": "JK Rowling",
#     "origin": "Yate, United Kingdom",
#     "birth date": "July 31, 1965",
#     "genre": "Fantasy",
#     "books": "Harry Potter and the Sorcerer's Stone",
#     "language": "English",
#     "author_image" : "https://upload.wikimedia.org/wikipedia/commons/5/5d/J._K._Rowling_2010.jpg",
#     },
#     {
#     "id" : 2,
#     "name": "Sir Arthur Conan Doyle",
#     "origin": "Edinburgh, Midlothian, Scotland",
#     "birth date": "May, 22 1859 - July 7, 1930",
#     "genre": "Mystery",
#     "books": "A Study in Scarlet",
#     "language": "English",
#     "author_image" : "https://upload.wikimedia.org/wikipedia/commons/b/bb/Conan_doyle.jpg",
#     },
#     {
#     "id" : 3,
#     "name": "Jane Austen",
#     "origin": "Steventon Rectory, Hampshire, England",
#     "birth date": "December 16, 1775 - July 18, 1817",
#     "genre": "Romance",
#     "books": "Pride and Prejudice",
#     "language": "English",
#     "author image": "https://upload.wikimedia.org/wikipedia/commons/6/65/J._Austen.jpg",
#     }]

# # data for three genres
# genres = [
#   {
#     "id": 1,
#     "genre_name": "Fiction",
#     "authors": ["Edward Abbey", "Chinua Achebr"],
#     "books": ["Things Fall Apart", "The Best of Edward Abbey"],
#     "profitability": "$10.93 billion",
#     "2021_publis_numbers": 2414,
#     "description": "Genre fiction, also known as popular fiction, is a term used in the book-trade for fictional works written with the intent of fitting into a specific literary genre, in order to appeal to readers and fans already familiar with that genre.",
#     "youtube_link": "https://www.youtube.com/watch?v=-_DN7l53J_c"
#   },
#   {
#     "id": 2,
#     "genre_name": "Biography & Autobiography",
#     "authors": ["Peter Ackroyd"],
#     "books": ["The Life of Thomas More"],
#     "profitability": "$30 million",
#     "2021_publis_numbers": 20,
#     "description": "Biographies are true stories about real people. They are set within a real historical framework with the unique social and political conditions that existed during the subject's life. Biographies can be about people who are alive or dead",
#     "youtube_link": "https://www.youtube.com/watch?v=4uVQ7A_uY1A"
#   },
#   {
#     "id": 3,
#     "genre_name": "Poetry",
#     "authors": ["John Agard, Conrad Aiken"],
#     "books": ["From the Devil's Pulpit", "The House of Dust: A Symphony"],
#     "profitability": "$12.3 million",
#     "2021_publis_numbers": 130,
#     "description": "Poetry is literature that evokes a concentrated imaginative awareness of experience or a specific emotional response through language chosen and arranged for its meaning, sound, and rhythm.",
#     "youtube_link": "https://www.youtube.com/watch?v=6SfPf-_OavY"
#   },
#   {
#     "id": 4,
#     "genre_name": "Literary Collections",
#     "authors": ["Chinua Achebe"],
#     "books": ["Home And Exile"],
#     "profitability": "$590 million",
#     "2021_publis_numbers" : 0,
#     "description": "A literary collection is any publication that consists of two or more independent works by different literary authors not written specifically for the publication in hand, regardless of whether the work has a collective title",
#     "youtube_link": "https://www.youtube.com/watch?v=NNIk2L7UBvY"
#   },
#   {
#     "id": 5,
#     "genre_name": "History",
#     "authors": ["James Agee"],
#     "books": ["Let Us Now Praise Famous Men", "Cotton Tenants"],
#     "profitability": "$736 million",
#     "2021_publis_numbers": 1433,
#     "description": "History is a genre that studies of change over time, and it covers all aspects of human society. Political, social, economic, scientific, technological, medical, cultural, intellectual, religious and military developments are all part of history",
#     "youtube_link": "https://www.youtube.com/watch?v=k7zzwEXHtA4"
#   },
#   {
#     "id": 6,
#     "genre_name": "Humor",
#     "authors": ["Douglas Adams"],
#     "books": ["The Deeper Meaning of Liff"],
#     "profitability": "",
#     "2021_publis_numbers": 55,
#     "description": "The mystery genre is a genre of fiction that follows a crime (like a murder or a disappearance) from the moment it is committed to the moment it is solved.",
#     "youtube_link": "https://www.youtube.com/watch?v=jE560nEckxg"
#   },
#   {
#     "id": 7,
#     "genre_name": "Drama",
#     "authors": ["Edward Albee"],
#     "books": ["Edward Albee's Fragments", "Counting the Ways and Listening"],
#     "profitability": "",
#     "2021_publis_numbers" : 21,
#     "description": "The mystery genre is a genre of fiction that follows a crime (like a murder or a disappearance) from the moment it is committed to the moment it is solved.",
#     "youtube_link": "https://www.youtube.com/watch?v=jE560nEckxg"
#   },
#   {
#     "id": 8,
#     "genre_name": "Art",
#     "authors": ["Edward Albee"],
#     "books": ["Obscure/reveal"],
#     "profitability": "$20 million",
#     "2021_publis_numbers" : 75,
#     "description": "Genre art is the pictorial representation in any of various media of scenes or events from everyday life, such as markets, domestic settings, interiors, parties, inn scenes, and street scenes.",
#     "youtube_link": "https://www.youtube.com/watch?v=3inY3LrjUBU"
#   },
#   {
#     "id": 9,
#     "genre_name": "Nature & Travel & Photography",
#     "authors": ["Edward Abbey"],
#     "books": ["Beyond the Wall"],
#     "profitability": "$343 million",
#     "2021_publis_numbers" : 0,
#     "description": "The genre of travel literature encompasses outdoor literature, guide books, nature writing, and travel memoirs",
#     "youtube_link": "https://www.youtube.com/watch?v=437_61pBGiI"
#   },
#   {
#     "id": 10,
#     "genre_name": "Performing Arts",
#     "authors": ["Forrest J. Ackerman"],
#     "books": ["Forrest J. Ackerman, Famous Monster of Filmland"],
#     "profitability": "$15 million",
#     "2021_publis_numbers" : 156,
#     "description": "Performance is a genre in which art is presented live, usually by the artist but sometimes with collaborators or performers. It has had a role in avant-garde art throughout the 20th century, playing an important part in anarchic movements such as Futurism and Dada.",
#     "youtube_link": "https://www.youtube.com/watch?v=kUk6rl5GSFE"
#   }
# ]


# @app.route("/api/books/<int:numbers>")
# def get_many_books(numbers):
#     """
#     returns data of given number of books in JSON format 
#     when the route is /api/books/numbers
#     """
#     return_list = []
#     for i in range(0, numbers):
#         return_list.append(books[i])
#     return jsonify(return_list)

# @app.route("/api/genres/<int:numbers>")
# def get_many_genres(numbers):
#     """
#     returns data of all given number of genres in JSON format 
#     when the route is /api/genres/numbers
#     """
#     return_list = []
#     for i in range(0, numbers):
#         return_list.append(genres[i])
#     return jsonify(return_list)

# @app.route("/api/books")
# def get_books():
#     """
#     returns data of all books in JSON format 
#     when the route is /api/books
#     """
#     return jsonify(books)

# @app.route("/api/authors")
# def get_authors():
#     """
#     returns data of all authors in JSON format
#     when the route is /api/authors
#     """
#     return jsonify(authors)

# @app.route("/api/genres")
# def get_genres():
#     """
#     returns data of all genres in JSON format
#     when the route is api/genres
#     """
#     return jsonify(genres)

# @app.route("/api/book/<int:id>")
# def get_one_book(id):
#     """
#     parameter id: the id of a specific book
#     returns the data about a specific book 
#     when the route is /api/book/<int:id>
#     """
#     for every_book in books:
#         if every_book['id'] == id:
#             return_book = [without_keys(every_book, "id")]
#             return jsonify(return_book)

# @app.route("/api/author/<int:id>")
# def get_one_author(id):
#     """
#     parameter id: the id of a specific author
#     returns the data about a specific author 
#     when the route is /api/author/<int:id>
#     """
#     for every_author in authors:
#         if every_author['id'] == id:
#             return_author = [without_keys(every_author, "id")]
#             return jsonify(return_author)

# @app.route("/api/genre/<int:id>")
# def get_one_genre(id):
#     """
#     parameter id: the id of a specific genre
#     returns the data about a specific genre 
#     when the route is /api/genre/<int:id>
#     """
#     for every_genre in genres:
#         if every_genre['id'] == id:
#             return_genre = [without_keys(every_genre, "id")]
#             return jsonify(return_genre)


# def without_keys(dic, keys):
#     """
#     paramter dic: the dictionary passed in
#     paramter keys: the keys we need to exclude from the given dictionary
#     Returns a new dictionary that does not contain certain keys in the old dictionary
#     """
#     return {x: dic[x] for x in dic if x not in keys}

# if __name__ == '__main__':
#     app.run(debug=True)
