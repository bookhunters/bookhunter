from flask import jsonify, request, Response;
import json;
from create_db import app, db, Book, Author, Genre
from Book import *
from Genre import *
from Author import *

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))
    return d


#template to fill in info
@app.route("/api/books")
def get_books():
    """
    returns data of all books in JSON format 
    when the route is /api/books
    """
    page = request.args.get("page", type=int)
    query = db.session.query(Book)

    # Searching
    q = request.args.get("q", default=None)
    query = search_books(query, q)

    # Filtering
    ratings = request.args.get("rt", type=int)
    book_page_count = request.args.get("pc", type=int)
    publication_date = request.args.get("pd", type=int)

    query = filter_books(query, ratings, book_page_count, publication_date)

    # Sorting
    sort = request.args.get("sort", default=None)
    query = sort_books(sort, query)

    # pagination
    books = query.paginate(page=page, per_page=10)
    book_list = []
    for one_book in books.items:
        book_list.append(row2dict(one_book))
    return json.dumps(book_list,ensure_ascii=False).encode('utf8').decode()


@app.route("/api/authors")
def get_authors():
    """
    returns data of all authors in JSON format
    when the route is /api/authors
    """
    page = request.args.get("page", type=int)
    # queries = request.args.to_dict(flat=False)
    query = db.session.query(Author)
    print(query)

    # Searching
    q = request.args.get("q", default=None)
    query = search_authors(query, q)

    # Filtering
    name = request.args.get("n", type=int)
    birth_date = request.args.get("d", type=int)

    query = filter_authors(query, name, birth_date)

    # Sorting
    sort = request.args.get("sort", default=None)
    print(sort)
    query = sort_authors(sort, query)

    # pagination
    authors = query.paginate(page=page, per_page=10)
    author_list = []
    for one_author in authors.items:
        author_list.append(row2dict(one_author))
    return json.dumps(author_list,ensure_ascii=False).encode('utf8').decode('utf8')


@app.route("/api/genres")
def get_genres():
    """
    returns data of all genres in JSON format
    when the route is api/genres
    """
    page = request.args.get("page", type=int)
    query = db.session.query(Genre)

    # Searching
    q = request.args.get("q", default=None)
    query = search_genres(query, q)

    # Filtering
    profits = request.args.get("pf", type=int)
    name = request.args.get("n", type=int)
    query = filter_genres(query, profits, name)

    # Sorting
    sort = request.args.get("sort", default=None)
    print(sort)
    query = sort_genres(sort, query)

    # pagination
    genres = query.paginate(page=page, per_page=10)
    genre_list = []
    for one_genre in genres.items:
        genre_list.append(row2dict(one_genre))
    return json.dumps(genre_list,ensure_ascii=False).encode('utf8').decode()

@app.route("/api/book/<int:id>")
def get_one_book(id):
    """
    parameter id: the id of a specific book
    returns the data about a specific book 
    when the route is /api/book/<int:id>
    """    
    book = Book.query.filter_by(book_id=id).first()
    if book:
        return json.dumps(row2dict(book),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Book with id '" + str(id) + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

@app.route("/api/book/<string:title>")
def get_book_byname(title):
    """
    parameter id: the id of a specific book
    returns the data about a specific book 
    when the route is /api/book/<int:id>
    """    
    book = Book.query.filter_by(book_title=title).first()
    if book:
        return json.dumps(row2dict(book),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Book with title '" + title + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

@app.route("/api/author/<int:id>")
def get_one_author(id):
    """
    parameter id: the id of a specific author
    returns the data about a specific author 
    when the route is /api/author/<int:id>
    """
    author = Author.query.filter_by(author_id=id).first()
    if author:
        return json.dumps(row2dict(author),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Author with id '" + str(id) + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

@app.route("/api/author/<string:name>")
def get_author_byname(name):
    """
    parameter id: the id of a specific author
    returns the data about a specific author 
    when the route is /api/author/<int:id>
    """
    author = Author.query.filter_by(author_name=name).first()
    if author:
        return json.dumps(row2dict(author),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Author with name '" + name + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

@app.route("/api/genre/<int:id>")
def get_one_genre(id):
    """
    parameter id: the id of a specific genre
    returns the data about a specific genre 
    when the route is /api/genre/<int:id>
    """
    genre = Genre.query.filter_by(genre_id=id).first()
    if genre:
        return json.dumps(row2dict(genre),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Genre with id '" + str(id) + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

@app.route("/api/genre/<string:name>")
def get_genre_byname(name):
    """
    parameter id: the id of a specific genre
    returns the data about a specific genre 
    when the route is /api/genre/<int:id>
    """
    genre = Genre.query.filter_by(genre_name=name).first()
    if genre:
        return json.dumps(row2dict(genre),ensure_ascii=False).encode('utf8').decode()
    else:
        response = Response(
            json.dumps({"error": "Genre with name '" + name + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response

if __name__ == '__main__':
    app.run(debug=True, port=5000)