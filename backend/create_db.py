import json
from models import app, db, Book, Genre, Author

#only a template to fill in info
def load_json(filename):
    with open(filename, encoding="utf8") as file:
        jsn = json.load(file)
        file.close()
    return jsn

def add_book_data():
    #book_list = load_json("sample_one_book.json")
    book_list = load_json("phase_three_sample_books.json")
    for one_book in book_list:
        id = one_book['id']
        genre = one_book['genre']
        author = one_book['authors'][0]
        #print(one_book)
        #print(author)
        someGenre = Genre.query.filter_by(genre_name=genre).first()
        genre_name = someGenre.genre_name
        #print(genre_name)
        someAuthor = Author.query.filter_by(author_name=author).first()
        #print(someAuthor)
        author_name = someAuthor.author_name
        #print("author name is" + author_name)
        isbn = one_book['isbn']
        language = one_book['language']
        publication_date = one_book['publication date']
        title = one_book['title']
        page_count = one_book['page count']
        ratings = one_book['ratings']
        description = one_book['description']
        book_image = one_book['book_image']
        new_book = Book(book_id = id, book_title = title, book_description = description, book_ISBN = isbn, 
        book_language = language, book_publication_date = publication_date, book_page_count = page_count, book_ratings = ratings,
        book_image = book_image, genre = someGenre, author = someAuthor, genre_showup = genre_name, author_showup = author_name) 
        db.session.add(new_book)

def add_genre_data():
    #genre_list = load_json("all_genre_info.json")
    genre_list = load_json("all_genre_info.json")
    for one_genre in genre_list:
        id = one_genre['id']
        genre_name = one_genre['genre_name']
        profitability = one_genre['profitability']
        description = one_genre['description']
        youtube_link = one_genre['youtube_link']
        new_genre = Genre(genre_id = id, genre_name = genre_name, genre_description = description, genre_youtube_link = youtube_link , genre_profitability = profitability)
        db.session.add(new_genre)

def add_author_data():
    #author_list = load_json("sample_authors_three.json")
    author_list = load_json("phase_three_sample_authors.json")
    for one_author in author_list:
        #print(one_author.keys())
        id = one_author['id']
        #print(id)
        #small: author_name = one_author['name']
        #large: author_name = one_author['author']
        author_name = one_author['Name']
        #print(author_name)
        #smallauthor_genres = one_author['genres']
        #largeauthor_genres = one_author['genre']
        author_genres = one_author['Genres']
        born = one_author['Born']
        education = one_author["Education"]
        nationality = one_author["Nationality"]
        description = one_author["Description"]
        image = one_author['image']
        new_author = Author(author_id = id, author_name = author_name, author_birth_date= born, author_image = image, author_nationality = nationality, author_description = description, author_education = education)
        db.session.add(new_author)
        db.session.commit()
        #print(author_genres)
        for i in range(len(author_genres)):
            current_genre = author_genres[i]
            #print(current_genre)
            someGenre = Genre.query.filter_by(genre_name=current_genre).first()
            #print(someGenre)
            new_author.author_genres.append(someGenre)
        db.session.commit()

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))
    return d

def set_genre_showup():
    for i in range(1, 22):
        someGenre = Genre.query.filter_by(genre_id=i).first()
        #print(someGenre.genre_name)
        a_list = []
        for one in someGenre.genre_books:
            if len(a_list) <= 6:
                a_list.append(row2dict(one)['book_title'])
            else:
                break
        #print(a_list)
        someGenre.books_showup = a_list
        #print(someGenre.books_showup)
        author_list = []
        for one_author in someGenre.familiar:
            if len(author_list) <= 6:
                author_list.append(row2dict(one_author)['author_name'])
            else:
                break
        someGenre.authors_showup = author_list
        #print(someGenre.authors_showup)
        db.session.commit()

def set_authors_showup():
    for i in range(1, 109):
        someAuthor = Author.query.filter_by(author_id=i).first()
        #print(someAuthor)
        a_list = []
        for one in someAuthor.author_genres:
            a_list.append(row2dict(one)['genre_name'])
        # print(a_list)
        someAuthor.genres_showup = a_list
        #print(someAuthor.genres_showup)
        book_list = []
        for one_author in someAuthor.author_books:
            book_list.append(row2dict(one_author)['book_title'])
        someAuthor.books_showup = book_list
        #print(someAuthor.books_showup)
        db.session.commit()

def add_data():
    db.drop_all()
    db.create_all()
    add_genre_data()
    db.session.commit()
    add_author_data()
    add_book_data()
    db.session.commit()
    set_genre_showup()
    set_authors_showup()
 
  # only for testing

    # for i in range(1, 9):
    #     someGenre = Genre.query.filter_by(genre_id=i).first()
    #     print(someGenre.genre_name)
    #     a_list = []
    #     for one in someGenre.familiar:
    #         a_list.append(row2dict(one))
    #     print(a_list)
add_data()
