from models import app, db, Book, Author, Genre
import flask_sqlalchemy
from sqlalchemy import or_, and_

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))
    return d

def search_genres(query, q):
    if q == None:
        return query

    queries = q.split(" ")
    print(queries)
    items = []
    for item in queries:
        items.append(Genre.genre_name.ilike("%{}%".format(item)))
        items.append(Genre.genre_description.ilike("%{}%".format(item)))
        try:
            items.append(Genre.genre_profitability.in_([int(item)]))
        except ValueError:
            pass
        items.append(Genre.books_showup.ilike("%{}%".format(item)))
        items.append(Genre.authors_showup.ilike("%{}%".format(item)))
    
    query = query.filter(or_(*tuple(items)))
    return query



def filter_genres(query,  profitability, name):
    if profitability != None:
        if profitability == 0:
            query = query.filter(Genre.genre_profitability < 100)
        elif profitability == 1:
            query = query.filter(Genre.genre_profitability.between(100, 500))
        else:
            query = query.filter(Genre.genre_profitability > 500)
    
    if name != None:
        if name == 0:
            query = query.filter(Genre.genre_name   < "F")
        elif name == 1:
            query = query.filter(and_(Genre.genre_name >= "F",Genre.genre_name < "L"))
        elif name == 2:
            query = query.filter(and_(Genre.genre_name >= "L",Genre.genre_name < "S"))
        else:
            query = query.filter(Genre.genre_name >= "S")

    return query


def sort_genres(sort, query):
    category = None
    if sort == "name":
        category = Genre.genre_name
    elif sort == "profits":
        category = Genre.genre_profitability
    elif sort == "authors":
        category = Genre.authors_showup
    elif sort == "books":
        category = Genre.books_showup
    else:
        category = Genre.genre_id
    return query.order_by(category)
