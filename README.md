Project name: BookHunter;

Members: Aditya Kuppa, Alyssa Rodriguez, Aman Kumar, Kenneth Knox, Revanth Muppana, Yizhou Li;

Link to our home page: http://www.bookhunter.me/

Postman Documentation about our APIs: https://documenter.getpostman.com/view/21599110/UzBqqRFd

Postman tests summary: https://interstellar-meteor-900907.postman.co/workspace/BookHunter~0c9d3ed5-7ef1-469c-aeb2-78c2238625fc/run/21599110-6a239871-9fb1-4f11-a0a9-9960671cdd08